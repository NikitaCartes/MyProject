#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

struct point_t
{
  double x;
  double y;
};

bool operator==(const point_t &point1, const point_t &point2);
double distance(const point_t &point1, const point_t &point2);

struct rectangle_t
{
  double width;
  double height;
  point_t pos;
};

bool operator==(const rectangle_t &rect1, const rectangle_t &rect2);

#endif
