#include "CompositeShape.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <stdexcept>

CompositeShape::CompositeShape(Shape *shape):
  array_(new Shape*[1]),
  length_(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("First shape can't be NULL");
  }
  array_[0] = shape;
}
void CompositeShape::addShape(Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape can't be NULL");
  }
  std::unique_ptr<Shape*[]> new_array(new Shape*[length_ + 1]);
  for (size_t i = 0; i < length_; i++)
  {
    new_array[i] = array_[i];
  }
  length_++;
  new_array[length_ - 1] = shape;
  array_.swap(new_array);
}
void CompositeShape::deleteShape(const size_t number)
{
  if (number >= length_)
  {
    throw std::invalid_argument("Constant out of range");
  }
  std::unique_ptr<Shape*[]> new_array(new Shape*[length_ - 1]);
  for (size_t i = 0; i < number; i++)
  {
    new_array[i] = array_[i];
  }
  for (size_t i = number; i < length_ - 1; i++)
  {
    new_array[i] = array_[i+1];
  }
  array_.swap(new_array);
  length_--;
}
double CompositeShape::getArea() const noexcept
{
  double area = 0;
  for (size_t i = 0; i < length_; i++)
  {
    area += array_[i]->getArea();
  }
  return area;
}
void CompositeShape::printInformation() const noexcept
{
  const point_t centre = getCentre();
  std::cout << "Info about composite shape:" << "\n" <<
    "centre.x = " << centre.x << "\n" <<
    "centre.y = " << centre.y << "\n" <<
    "About each figure:\n" << std::endl;
  for (size_t i = 0; i < length_; i++)
  {
    array_[i]->printInformation();
    std::cout << "\n";
  }
}
rectangle_t CompositeShape::getFrameRect() const noexcept
{
  rectangle_t rect = array_[0]->getFrameRect();
  double max_X = rect.pos.x + (rect.width / 2);
  double max_Y = rect.pos.y + (rect.height / 2);
  double min_X = rect.pos.x - (rect.width / 2);
  double min_Y = rect.pos.y - (rect.height / 2);
  for (size_t i = 0; i < length_; i++)
  {
    rect = array_[i]->getFrameRect();
    max_X = std::max(max_X, rect.pos.x + (rect.width / 2));
    max_Y = std::max(max_Y, rect.pos.y + (rect.height / 2));
    min_X = std::min(min_X, rect.pos.x - (rect.width / 2));
    min_Y = std::min(min_Y, rect.pos.y - (rect.height / 2));
  }
  return { (max_X - min_X) , (max_Y - min_Y) ,{ (max_X + min_X) / 2 , (max_Y + min_Y) / 2 } };
}
point_t CompositeShape::getCentre() const noexcept
{
  return getFrameRect().pos;
}
size_t CompositeShape::getLength() const noexcept
{
  return length_;
}
void CompositeShape::move(const point_t &pos) noexcept
{
  const point_t centre = getCentre();
  const double dx = pos.x - centre.x;
  const double dy = pos.y - centre.y;
  for (size_t i = 0; i < length_; i++)
  {
    array_[i]->move(dx, dy);
  }
}
void CompositeShape::move(const double dx, const double dy) noexcept
{
  for (size_t i = 0; i < length_; i++)
  {
    array_[i]->move(dx, dy);
  }
}
void CompositeShape::scale(const double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("k can't be lesser or equal to 0");
  }
  const point_t centre = getCentre();
  for (size_t i = 0; i < length_; i++)
  {
    const point_t t_centre = array_[i]->getCentre();
    array_[i]->move({ centre.x + (t_centre.x - centre.x)*k,
      centre.y + (t_centre.y - centre.y)*k });
    array_[i]->scale(k);
  }
}
void CompositeShape::rotate(double angle) noexcept
{
  rotate(angle, getCentre());
}
void CompositeShape::rotate(double angle, const point_t &pos) noexcept
{
  for (size_t i = 0; i < length_; i++)
  {
    array_[i]->rotate(angle, pos);
  }
}
Shape& CompositeShape::operator[](const size_t i) const
{
  if (i >= length_)
  {
    throw std::invalid_argument("Constant out of range");
  }
  return *array_[i];
}
