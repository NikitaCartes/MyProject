#include "base-types.hpp"
#include <math.h>

bool operator==(const point_t & point1, const point_t & point2)
{
  return ((fabs(point2.x - point1.x) < 0.0001) && (fabs(point2.y - point1.y) < 0.0001));
}

double distance(const point_t & point1, const point_t & point2)
{
  return sqrt(pow(point2.x - point1.x, 2) + pow(point2.y - point1.y, 2));
}

bool operator==(const rectangle_t & rect1, const rectangle_t & rect2)
{
  return ((rect1.pos == rect2.pos) &&
    (fabs(rect1.width - rect2.width) < 0.0001) &&
    (fabs(rect1.height - rect2.height) < 0.0001));
}
