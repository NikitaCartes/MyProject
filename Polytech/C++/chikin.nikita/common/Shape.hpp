#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <iostream>

class Shape
{
public:
  virtual double getArea() const = 0;
  virtual rectangle_t getFrameRect() const = 0;
  virtual void printInformation() const = 0;
  virtual point_t getCentre() const = 0;
  virtual void move(const point_t &pos) = 0;
  virtual void move(const double, const double) = 0;
  virtual void scale(const double) = 0;
  virtual void rotate(const double) = 0;
  virtual void rotate(double, const point_t &pos) = 0;
};

#endif
