#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "Shape.hpp"
#include <memory>

class CompositeShape : public Shape
{
public:
  CompositeShape(Shape *shape);
  CompositeShape(CompositeShape &compositeshape) = delete;
  void addShape(Shape *shape);
  void addShape(CompositeShape &compositeshape) = delete;
  void deleteShape(const size_t);
  double getArea() const noexcept;
  void printInformation() const noexcept;
  point_t getCentre() const noexcept;
  size_t getLength() const noexcept;
  rectangle_t getFrameRect() const noexcept;
  void move(const point_t &pos) noexcept;
  void move(const double, const double) noexcept;
  void scale(const double);
  void rotate(const double) noexcept;
  void rotate(double, const point_t &pos) noexcept;
  Shape& operator[](const size_t) const;
private:
  std::unique_ptr<Shape*[]> array_;
  size_t length_;
};

#endif
