#include "Polygon.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <stdexcept>

Polygon::Polygon(point_t *vertexes, const size_t number) :
  vertexes_(new point_t*[number + 1]),
  length_(number)
{
  if (vertexes == nullptr)
  {
    throw std::invalid_argument("Vertexes can't be NULL");
  }
  if (number < 3)
  {
    throw std::invalid_argument("Number of vertexes can't be lesser 4");
  }
  for (size_t i = 0; i < number; i++)
  {
    vertexes_[i] = &vertexes[i];
  }
  if (getArea() == 0)
  {
    throw std::invalid_argument("Polygon can't be degenerate");
  }
  if (!isConvex())
  {
    throw std::invalid_argument("Polygon must be convex");
  }
}
void Polygon::addVertex(point_t &vertex)
{
  addVertexAfter(vertex, length_);
}
void Polygon::addVertex(point_t *vertexes, const size_t number)
{
  addVertexAfter(vertexes, length_, number);
}
void Polygon::addVertexAfter(point_t &vertex, const size_t after_this)
{
  point_t points[] = {vertex};
  addVertexAfter(points, after_this, 1);
}
void Polygon::addVertexAfter(point_t *vertexes, const size_t after_this, const size_t number)
{
  if (vertexes == nullptr)
  {
    throw std::invalid_argument("Vertexes can't be NULL");
  }
  std::unique_ptr<point_t*[]> new_array(new point_t*[length_ + number]);
  for (size_t i = 0; i < after_this + 1; i++)
  {
    new_array[i] = vertexes_[i];
  }
  for (size_t i = after_this; i < after_this + number; i++)
  {
    new_array[i] = &vertexes[i - after_this];
  }
  for (size_t i = after_this + number; i < length_ + number; i++)
  {
    new_array[i] = vertexes_[i - number];
  }
  length_ += number;
  vertexes_.swap(new_array);
  if (!isConvex())
  {
    throw std::invalid_argument("Can't add this points");
  }
}
void Polygon::deleteVertex(const size_t number)
{
  if (number >= length_)
  {
    throw std::invalid_argument("Constant out of range");
  }
  if (length_ == 3)
  {
    throw std::invalid_argument("Number of vertexes can't be lesser 4");
  }
  std::unique_ptr<point_t*[]> new_array(new point_t*[length_ - 1]);
  for (size_t i = 0; i < number; i++)
  {
    new_array[i] = vertexes_[i];
  }
  for (size_t i = number; i < length_ - 1; i++)
  {
    new_array[i] = vertexes_[i + 1];
  }
  vertexes_.swap(new_array);
  length_--;
  if (getArea() == 0)
  {
    throw std::invalid_argument("Polygon can't be degenerate");
  }
}
double Polygon::getArea() const noexcept
{
  double area = 0;
  area += vertexes_[0]->x * (vertexes_[1]->y - vertexes_[length_ - 1]->y);
  for (size_t i = 1; i < length_ - 1; i++)
  {
    area += vertexes_[i]->x * (vertexes_[i + 1]->y - vertexes_[i - 1]->y);
  }
  area += vertexes_[length_ - 1]->x * (vertexes_[0]->y - vertexes_[length_ - 2]->y);
  return 0.5*fabs(area);
}
bool Polygon::isConvex() noexcept
{
  if (length_ == 3)
  {
    return true;
  }
  for (size_t i = 3; i < length_; i++)
  {
    for (size_t j = 0; j < i - 2; j++)
    {
      const double flag = ((vertexes_[i - 1]->y - vertexes_[i]->y) * vertexes_[j]->x +
        (vertexes_[i]->x - vertexes_[i - 1]->x) * vertexes_[j]->y +
        (vertexes_[i - 1]->x * vertexes_[i]->y - vertexes_[i]->x * vertexes_[i - 1]->y)) *
        ((vertexes_[i - 1]->y - vertexes_[i]->y) * vertexes_[j + 1]->x +
        (vertexes_[i]->x - vertexes_[i - 1]->x) * vertexes_[j + 1]->y +
        (vertexes_[i - 1]->x * vertexes_[i]->y - vertexes_[i]->x * vertexes_[i - 1]->y));
      if (flag == 0)
      {
        try
        {
          deleteVertex(i - 1);
          return isConvex();
        }
        catch (...)
        {
          return false;
        }
      }
      if (flag < 0)
      {
        try
        {
          deleteVertex(i);
          return isConvex();
        }
        catch (...)
        {
          return false;
        }
      }
    }
  }
  return true;
}
void Polygon::printInformation() const noexcept
{
  const point_t centre = getCentre();
  std::cout << "Info about polygon:" << "\n" <<
    "centre.x = " << centre.x << "\n" <<
    "centre.y = " << centre.y << "\n" <<
    "About each vertex:" << std::endl;
  for (size_t i = 0; i < length_; i++)
  {
    std::cout << i << ": {" << vertexes_[i]->x << "," << vertexes_[i]->y << "}" << std::endl;
  }
}
rectangle_t Polygon::getFrameRect() const noexcept
{
  double max_X = vertexes_[0]->x;
  double max_Y = vertexes_[0]->y;
  double min_X = vertexes_[0]->x;
  double min_Y = vertexes_[0]->y;
  for (size_t i = 0; i < length_; i++)
  {
    max_X = std::max(max_X, vertexes_[i]->x);
    max_Y = std::max(max_Y, vertexes_[i]->y);
    min_X = std::min(min_X, vertexes_[i]->x);
    min_Y = std::min(min_Y, vertexes_[i]->y);
  }
  return { (max_X - min_X) , (max_Y - min_Y) ,{ (max_X + min_X) / 2 , (max_Y + min_Y) / 2 } };
}
point_t Polygon::getCentre() const noexcept
{
  double x = 0;
  double y = 0;
  for (size_t i = 0; i < length_; i++)
  {
    x += vertexes_[i]->x;
    y += vertexes_[i]->y;
  }
  return {x/length_, y/length_};
}
size_t Polygon::getLength() const noexcept
{
  return length_;
}
void Polygon::move(const point_t &pos) noexcept
{
  const point_t centre = getCentre();
  const double dx = pos.x - centre.x;
  const double dy = pos.y - centre.y;
  for (size_t i = 0; i < length_; i++)
  {
    vertexes_[i]->x += dx;
    vertexes_[i]->y += dy;
  }
}
void Polygon::move(const double dx, const double dy) noexcept
{
  for (size_t i = 0; i < length_; i++)
  {
    vertexes_[i]->x += dx;
    vertexes_[i]->y += dy;
  }
}
void Polygon::scale(const double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("k can't be lesser or equal to 0");
  }
  const point_t centre = getCentre();
  for (size_t i = 0; i < length_; i++)
  {
    vertexes_[i]->x = centre.x + (vertexes_[i]->x - centre.x)*k;
    vertexes_[i]->y = centre.y + (vertexes_[i]->y - centre.y)*k;
  }
}
void Polygon::rotate(double angle) noexcept
{
  rotate(angle, getCentre());
}
void Polygon::rotate(double angle, const point_t &pos) noexcept
{
  angle = (angle*M_PI) / 180;
  const double sin_angle = sin(angle);
  const double cos_angle = cos(angle);
  for (size_t i = 0; i < length_; i++)
  {
    const point_t tempPoint = *vertexes_[i];
    vertexes_[i]->x = pos.x + (tempPoint.x - pos.x)*cos_angle - (tempPoint.y - pos.y)*sin_angle;
    vertexes_[i]->y = pos.y + (tempPoint.x - pos.x)*sin_angle + (tempPoint.y - pos.y)*cos_angle;
  }
}
point_t& Polygon::operator[](const size_t i) const
{
  if (i >= length_)
  {
    throw std::invalid_argument("Constant out of range");
  }
  return *vertexes_[i];
}
