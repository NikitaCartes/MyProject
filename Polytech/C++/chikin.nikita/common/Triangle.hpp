#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "Shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t &A, const point_t &B, const point_t &C);
  double getArea() const noexcept;
  void printInformation() const noexcept;
  point_t getCentre() const noexcept;
  rectangle_t getFrameRect() const noexcept;
  void move(const point_t &pos) noexcept;
  void move(const double, const double) noexcept;
  void scale(const double);
  void rotate(const double) noexcept;
  void rotate(double, const point_t &pos) noexcept;
private:
  point_t pointA_;
  point_t pointB_;
  point_t pointC_;
};

#endif
