#include "Triangle.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <stdexcept>

Triangle::Triangle(const point_t &A, const point_t &B, const point_t &C):
  pointA_(A),
  pointB_(B),
  pointC_(C)
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Triangle can't be degenerate");
  }
}
void Triangle::printInformation() const noexcept
{
  std::cout << "Info about triangle:" << "\n" <<
    "centre.x = " << (pointA_.x + pointB_.x + pointC_.x) / 3 << "\n" <<
    "centre.y = " << (pointA_.y + pointB_.y + pointC_.y) / 3 << "\n" <<
    "A.x = " << pointA_.x << " A.y = " << pointA_.y << "\n" <<
    "B.x = " << pointB_.x << " B.y = " << pointB_.y << "\n" <<
    "C.x = " << pointC_.x << " C.y = " << pointC_.y << "\n" << std::endl;
}
point_t Triangle::getCentre() const noexcept
{
  return { (pointA_.x + pointB_.x + pointC_.x) / 3.0 , (pointA_.y + pointB_.y + pointC_.y) / 3.0 };
}
double Triangle::getArea() const noexcept
{
  return (fabs(((pointA_.x - pointC_.x) * (pointB_.y - pointC_.y)) - ((pointA_.y - pointC_.y) * (pointB_.x - pointC_.x))) / 2);
}
rectangle_t Triangle::getFrameRect() const noexcept
{
  const double max_X = std::max(pointA_.x, std::max(pointB_.x, pointC_.x));
  const double max_Y = std::max(pointA_.y, std::max(pointB_.y, pointC_.y));
  const double min_X = std::min(pointA_.x, std::min(pointB_.x, pointC_.x));
  const double min_Y = std::min(pointA_.y, std::min(pointB_.y, pointC_.y));
  return { (max_X - min_X) , (max_Y - min_Y) ,{ (max_X + min_X) / 2 , (max_Y + min_Y) / 2 } };
}
void Triangle::move(const point_t &pos) noexcept
{
  const double dx = pos.x - (pointA_.x + pointB_.x + pointC_.x) / 3;
  const double dy = pos.y - (pointA_.y + pointB_.y + pointC_.y) / 3;
  move(dx, dy);
}
void Triangle::move(const double dx, const double dy) noexcept
{
  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;
  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
}
void Triangle::scale(const double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("k can't be lesser or equal to 0");
  }
  const double x = (pointA_.x + pointB_.x + pointC_.x) / 3;
  const double y = (pointA_.y + pointB_.y + pointC_.y) / 3;
  pointA_.x = x + (pointA_.x - x)*k;
  pointA_.y = y + (pointA_.y - y)*k;
  pointB_.x = x + (pointB_.x - x)*k;
  pointB_.y = y + (pointB_.y - y)*k;
  pointC_.x = x + (pointC_.x - x)*k;
  pointC_.y = y + (pointC_.y - y)*k;
}
void Triangle::rotate(double angle) noexcept
{
  rotate(angle, getCentre());
}
void Triangle::rotate(double angle, const point_t &pos) noexcept
{
  angle = (angle*M_PI) / 180;
  const double sin_angle = sin(angle);
  const double cos_angle = cos(angle);
  const point_t tempPoint1 = pointA_;
  pointA_.x = pos.x + (tempPoint1.x - pos.x)*cos_angle - (tempPoint1.y - pos.y)*sin_angle;
  pointA_.y = pos.y + (tempPoint1.x - pos.x)*sin_angle + (tempPoint1.y - pos.y)*cos_angle;
  const point_t tempPoint2 = pointB_;
  pointB_.x = pos.x + (tempPoint2.x - pos.x)*cos_angle - (tempPoint2.y - pos.y)*sin_angle;
  pointB_.y = pos.y + (tempPoint2.x - pos.x)*sin_angle + (tempPoint2.y - pos.y)*cos_angle;
  const point_t tempPoint3 = pointC_;
  pointC_.x = pos.x + (tempPoint3.x - pos.x)*cos_angle - (tempPoint3.y - pos.y)*sin_angle;
  pointC_.y = pos.y + (tempPoint3.x - pos.x)*sin_angle + (tempPoint3.y - pos.y)*cos_angle;
}
