#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "Shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(const point_t &pointA, const point_t &pointB,
    const point_t &pointC, const point_t &pointD);
  Rectangle(const double, const double, const point_t &centre);
  double getArea() const noexcept;
  void printInformation() const noexcept;
  point_t getCentre() const noexcept;
  rectangle_t getFrameRect() const noexcept;
  void move(const point_t &pos) noexcept;
  void move(const double, const double) noexcept;
  void scale(const double);
  void rotate(const double) noexcept;
  void rotate(double, const point_t &pos) noexcept;
private:
  point_t pointA_;
  point_t pointB_;
  point_t pointC_;
  point_t pointD_;
};

#endif
