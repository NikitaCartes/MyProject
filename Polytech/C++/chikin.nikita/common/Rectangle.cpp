#include "Rectangle.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <stdexcept>

Rectangle::Rectangle(const point_t &pointA, const point_t &pointB,
  const point_t &pointC, const point_t &pointD):
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC),
  pointD_(pointD)
{
  const double AC = distance(pointA_, pointC_);
  const double BD = distance(pointB_, pointD_);
  const double AB = distance(pointA_, pointB_);
  const double BC = distance(pointB_, pointC_);
  const double CD = distance(pointC_, pointD_);
  const double DA = distance(pointD_, pointA_);
  if ((AC != BD) || (AB != CD) || (BC != DA))
  {
    throw std::invalid_argument("This shape isn't rectangle");
  }
  if (BC == 0)
  {
    throw std::invalid_argument("width can't be equal to 0");
  }
  if (AB == 0)
  {
    throw std::invalid_argument("height can't be equal to 0");
  }
}
Rectangle::Rectangle(const double width, const double height, const point_t &centre):
  pointA_({ centre.x - width / 2, centre.y - height / 2 }),
  pointB_({ centre.x - width / 2, centre.y + height / 2 }),
  pointC_({ centre.x + width / 2, centre.y + height / 2 }),
  pointD_({ centre.x + width / 2, centre.y - height / 2 })
{
  if (width <= 0)
  {
    throw std::invalid_argument("width can't be lesser or equal to 0");
  }
  if (height <= 0)
  {
    throw std::invalid_argument("height can't be lesser or equal to 0");
  }
}
void Rectangle::printInformation() const noexcept
{
  std::cout << "Info about rectangle:" << "\n" <<
    "centre.x = " << (pointA_.x + pointB_.x + pointC_.x + pointD_.x) / 4 << "\n" <<
    "centre.y = " << (pointA_.y + pointB_.y + pointC_.y + pointD_.y) / 4 << "\n" <<
    "A.x = " << pointA_.x << " A.y = " << pointA_.y << "\n" <<
    "B.x = " << pointB_.x << " B.y = " << pointB_.y << "\n" <<
    "C.x = " << pointC_.x << " C.y = " << pointC_.y << "\n" <<
    "D.x = " << pointD_.x << " D.y = " << pointD_.y << std::endl;
}
point_t Rectangle::getCentre() const noexcept
{
  return { (pointA_.x + pointB_.x + pointC_.x + pointD_.x) / 4 ,
    (pointA_.y + pointB_.y + pointC_.y + pointD_.y) / 4 };
}
double Rectangle::getArea() const noexcept
{
  const double AB = distance(pointA_, pointB_);
  const double BC = distance(pointB_, pointC_);
  return (AB * BC);
}
rectangle_t Rectangle::getFrameRect() const noexcept
{
  const double max_X = std::max(pointA_.x, std::max(pointB_.x, std::max(pointC_.x, pointD_.x)));
  const double max_Y = std::max(pointA_.y, std::max(pointB_.y, std::max(pointC_.y, pointD_.y)));
  const double min_X = std::min(pointA_.x, std::min(pointB_.x, std::min(pointC_.x, pointD_.x)));
  const double min_Y = std::min(pointA_.y, std::min(pointB_.y, std::min(pointC_.y, pointD_.y)));
  return { (max_X - min_X) , (max_Y - min_Y) ,{ (max_X + min_X) / 2 , (max_Y + min_Y) / 2 } };
}
void Rectangle::move(const point_t &pos) noexcept
{
  const double dx = pos.x - (pointA_.x + pointB_.x + pointC_.x + pointD_.x) / 4;
  const double dy = pos.y - (pointA_.y + pointB_.y + pointC_.y + pointD_.y) / 4;
  move(dx, dy);
}
void Rectangle::move(const double dx, const double dy) noexcept
{
  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;
  pointD_.x += dx;
  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
  pointD_.y += dy;
}
void Rectangle::scale(const double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("k can't be lesser or equal to 0");
  }
  const double x = (pointA_.x + pointB_.x + pointC_.x + pointD_.x) / 4;
  const double y = (pointA_.y + pointB_.y + pointC_.y + pointD_.y) / 4;
  pointA_.x = x + (pointA_.x - x)*k;
  pointA_.y = y + (pointA_.y - y)*k;
  pointB_.x = x + (pointB_.x - x)*k;
  pointB_.y = y + (pointB_.y - y)*k;
  pointC_.x = x + (pointC_.x - x)*k;
  pointC_.y = y + (pointC_.y - y)*k;
  pointD_.x = x + (pointD_.x - x)*k;
  pointD_.y = y + (pointD_.y - y)*k;
}
void Rectangle::rotate(double angle) noexcept
{
  rotate(angle, getCentre());
}
void Rectangle::rotate(double angle, const point_t &pos) noexcept
{
  angle = (angle*M_PI) / 180;
  const double sin_angle = sin(angle);
  const double cos_angle = cos(angle);
  const point_t tempPoint1 = pointA_;
  pointA_.x = pos.x + (tempPoint1.x - pos.x)*cos_angle - (tempPoint1.y - pos.y)*sin_angle;
  pointA_.y = pos.y + (tempPoint1.x - pos.x)*sin_angle + (tempPoint1.y - pos.y)*cos_angle;
  const point_t tempPoint2 = pointB_;
  pointB_.x = pos.x + (tempPoint2.x - pos.x)*cos_angle - (tempPoint2.y - pos.y)*sin_angle;
  pointB_.y = pos.y + (tempPoint2.x - pos.x)*sin_angle + (tempPoint2.y - pos.y)*cos_angle;
  const point_t tempPoint3 = pointC_;
  pointC_.x = pos.x + (tempPoint3.x - pos.x)*cos_angle - (tempPoint3.y - pos.y)*sin_angle;
  pointC_.y = pos.y + (tempPoint3.x - pos.x)*sin_angle + (tempPoint3.y - pos.y)*cos_angle;
  const point_t tempPoint4 = pointD_;
  pointD_.x = pos.x + (tempPoint4.x - pos.x)*cos_angle - (tempPoint4.y - pos.y)*sin_angle;
  pointD_.y = pos.y + (tempPoint4.x - pos.x)*sin_angle + (tempPoint4.y - pos.y)*cos_angle;
}
