#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "Shape.hpp"
#include <memory>

class Matrix
{
public:
  Matrix(const Shape *shape);
  Matrix(Matrix &compositeshape) = delete;
  void addShape(const Shape *shape);
  void addShape(Matrix &compositeshape) = delete;
  void deleteShape(const size_t, const size_t);
  void printInformation() const noexcept;
  const Shape** operator[](const size_t) const;
  ~Matrix();
private:
  const Shape ** matrix_;
  size_t layers_, length_;
  bool intersections(const Shape * shape1, const Shape * shape2) const noexcept;
};

#endif
