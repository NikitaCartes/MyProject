#include "Matrix.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <stdexcept>

Matrix::Matrix(const Shape * shape) :
  matrix_(new const Shape *[1]),
  layers_(1),
  length_(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("First shape can't be NULL");
  }
  matrix_[0] = shape;
}

void Matrix::addShape(const Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape can't be NULL");
  }
  size_t i = layers_;
  size_t j = length_ - 1;
  while ((i != 0) && (!intersections(matrix_[(i - 1)*length_ + j], shape)))
  {
    (j != 0) ? j-- : (i--, j = length_ - 1);
  }
  if (i == layers_)
  {
    layers_++;
    const Shape ** new_matrix = new const Shape *[length_ * layers_];
    i = 0;
    while (i < (layers_ - 1) * length_)
    {
      new_matrix[i] = matrix_[i];
      i++;
    }
    new_matrix[i++] = shape;
    while (i < length_ * layers_)
    {
      new_matrix[i++] = nullptr;
    }
    delete[] matrix_;
    matrix_ = new_matrix;
  }
  else
  {
    j = 0;
    while ((j < length_) && (matrix_[i * length_ + j] != nullptr))
    {
      j++;
    }
    if (j == length_)
    {
      length_++;
      const Shape ** new_matrix = new const Shape *[length_ * layers_];
      j = 0;
      size_t iter = 0;
      while (j < length_ * layers_)
      {
        if ((j + 1) % length_ == 0)
        {
          if ((i + 1) == ((j + 1) / length_))
          {
            new_matrix[j] = shape;
          }
          else
          {
            new_matrix[j] = nullptr;
          }
        }
        else
        {
          new_matrix[j] = matrix_[iter++];
        }
        j++;
      }
      delete[] matrix_;
      matrix_ = new_matrix;
    }
    else
    {
      matrix_[i * length_ + j] = shape;
    }
  }
}
void Matrix::deleteShape(const size_t i, const size_t j)
{
  if (i >= layers_)
  {
    throw std::invalid_argument("Constant out of range");
  }
  if (j >= length_)
  {
    throw std::invalid_argument("Constant out of range");
  }
  size_t k = 0;
  while ((matrix_[k] == nullptr) || (k == i * length_ + j))
  {
    k++;
  }
  Matrix new_matrix(matrix_[k]);
  for (k = k + 1; k < i * length_ + j; k++)
  {
    try
    {
      new_matrix.addShape(matrix_[k]);
    }
    catch (...)
    {
      continue;
    }
  }
  for (k = i * length_ + j + 1; k < layers_ * length_; k++)
  {
    try
    {
      new_matrix.addShape(matrix_[k]);
    }
    catch (...)
    {
      continue;
    }
  }
}
void Matrix::printInformation() const noexcept
{
  for (size_t i = 0; i < layers_; i++)
  {
    for (size_t j = 0; j < length_; j++)
    {
      std::cout << "[" << i << "][" << j << "]:";
      if (matrix_[i * length_ + j] == nullptr)
      {
        std::cout << "nullptr" << std::endl;
      }
      else
      {
        std::cout << "\n";
        matrix_[i * length_ + j]->printInformation();
      }
    }
    std::cout << '\n';
  }
}
const Shape** Matrix::operator[](const size_t i) const
{
  if (i > layers_)
  {
    throw std::invalid_argument("Constant out of range");
  }
  return (matrix_ + i * length_);
}

Matrix::~Matrix()
{
  delete[] matrix_;
}

bool Matrix::intersections(const Shape * shape1, const Shape * shape2) const noexcept
{
  if ((shape1 == nullptr) || (shape2 == nullptr))
  {
    return false;
  }
  const rectangle_t rect1 = shape1->getFrameRect();
  const rectangle_t rect2 = shape2->getFrameRect();
  const point_t point1{ (rect1.pos.x - rect1.width / 2.0), (rect1.pos.y - rect1.height / 2.0) };
  const point_t point2{ (rect1.pos.x + rect1.width / 2.0), (rect1.pos.y + rect1.height / 2.0) };
  const point_t point3{ (rect2.pos.x - rect2.width / 2.0), (rect2.pos.y - rect2.height / 2.0) };
  const point_t point4{ (rect2.pos.x + rect2.width / 2.0), (rect2.pos.y + rect2.height / 2.0) };
  if (((point1.x > point3.x) && (point1.x < point4.x)) ||
    ((point2.x > point3.x) && (point2.x < point4.x)) ||
    ((point2.y > point3.y) && (point2.y < point4.y)) ||
    ((point1.y > point3.y) && (point1.y < point4.y)) ||
    ((rect1.pos.y > point3.y) && (rect1.pos.y < point4.y)) ||
    ((rect1.pos.x > point3.x) && (rect1.pos.x < point4.x)) ||
    ((point3.x > point1.x) && (point3.x < point2.x)) ||
    ((point4.x > point1.x) && (point4.x < point2.x)) ||
    ((point4.y > point1.y) && (point4.y < point2.y)) ||
    ((point3.y > point1.y) && (point3.y < point2.y)) ||
    ((rect2.pos.y > point1.y) && (rect2.pos.y < point2.y)) ||
    ((rect2.pos.x > point1.x) && (rect2.pos.x < point2.x)))
  {
    return true;
  }
  return false;
}
