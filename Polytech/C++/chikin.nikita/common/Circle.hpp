#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "Shape.hpp"

class Circle : public Shape
{
public:
  Circle(const point_t &pos, const double);
  double getArea() const noexcept;
  void printInformation() const noexcept;
  point_t getCentre() const noexcept;
  rectangle_t getFrameRect() const noexcept;
  void move(const point_t &pos) noexcept;
  void move(const double, const double) noexcept;
  void scale(const double);
  void rotate(const double) noexcept;
  void rotate(double, const point_t &pos) noexcept;
private:
  point_t pos_;
  double radius_;
};

#endif
