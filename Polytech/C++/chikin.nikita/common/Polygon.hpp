#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "Shape.hpp"
#include <memory>

class Polygon : public Shape
{
public:
  Polygon(point_t *vertexes, const size_t);
  void addVertex(point_t &vertex);
  void addVertex(point_t *vertexes, const size_t);
  void addVertexAfter(point_t &vertex, const size_t);
  void addVertexAfter(point_t *vertexes, const size_t, const size_t);
  void deleteVertex(const size_t);
  double getArea() const noexcept;
  void printInformation() const noexcept;
  point_t getCentre() const noexcept;
  size_t getLength() const noexcept;
  rectangle_t getFrameRect() const noexcept;
  void move(const point_t &pos) noexcept;
  void move(const double, const double) noexcept;
  void scale(const double);
  void rotate(const double) noexcept;
  void rotate(double, const point_t &pos) noexcept;
  point_t& operator[](const size_t) const;
private:
  std::unique_ptr<point_t*[]> vertexes_;
  size_t length_;
  bool isConvex() noexcept;
};

#endif
