#include "Circle.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdexcept>

Circle::Circle(const point_t &pos, const double radius):
  pos_(pos),
  radius_(radius)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("radius can't be lesser or equal to 0");
  }
}
double Circle::getArea() const noexcept
{
  return (pow(radius_, 2)*M_PI);
}
void Circle::printInformation() const noexcept
{
  std::cout << "Info about circle:" << "\n" <<
    "centre.x = " << pos_.x << "\n" <<
    "centre.y = " << pos_.y << "\n" <<
    "radius = " << radius_ << std::endl;
}
point_t Circle::getCentre() const noexcept
{
  return pos_;
}
rectangle_t Circle::getFrameRect() const noexcept
{
  return { 2*radius_ , 2*radius_ , { pos_.x , pos_.y} };
}
void Circle::move(const point_t &pos) noexcept
{
  pos_.x = pos.x;
  pos_.y = pos.y;
}
void Circle::move(const double dx, const double dy) noexcept
{
  pos_.x += dx;
  pos_.y += dy;
}
void Circle::scale(const double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("k can't be lesser or equal to 0");
  }
  radius_ *= k;
}
void Circle::rotate(double) noexcept
{
}
void Circle::rotate(double angle, const point_t &pos) noexcept
{
  angle = (angle*M_PI) / 180;
  const double sin_angle = sin(angle);
  const double cos_angle = cos(angle);
  const point_t tempPoint = pos_;
  pos_.x = pos.x + (tempPoint.x - pos.x)*cos_angle - (tempPoint.y - pos.y)*sin_angle;
  pos_.y = pos.y + (tempPoint.x - pos.x)*sin_angle + (tempPoint.y - pos.y)*cos_angle;
}
