#include "Source.hpp"
#include "Punkts.hpp"

int main(int args, char *argv[])
{
  if(args != 2)
  {
    std::cerr << "Incorrect number of input parameters";
    return 1;
  }
  try
  {
    switch(std::stoi(argv[1]))
    {
    case 1:
      Punkt1();
      break;
    case 2:
      Punkt2();
      break;
    default:
      std::cerr << "Incorrect parameters";
      return 1;
    }
  }
  catch (std::ios_base::failure & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  catch (std::invalid_argument & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  return 0;
}





