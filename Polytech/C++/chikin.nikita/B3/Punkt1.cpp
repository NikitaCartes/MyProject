#include "Punkts.hpp"
#include <sstream>
#include "Source.hpp"

void Punkt1()
{
  Queue queue;

  std::string line;
  while(std::getline(std::cin, line))
  {
    std::stringstream stream(line);
    std::string command;
    stream >> command;

    if(command == "add")
    {
      std::string priority;
      stream >> priority;

      std::string data;
      stream.ignore();
      std::getline(stream, data);

      if(data.empty())
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
      else if(priority == "high")
      {
        queue.PutElement(data, ElementPriority::HIGH);
      }
      else if(priority == "normal")
      {
        queue.PutElement(data, ElementPriority::NORMAL);
      }
      else if(priority == "low")
      {
        queue.PutElement(data, ElementPriority::LOW);
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
    }
    else if(command == "accelerate" && stream.eof())
    {
      queue.Accelerate();
    }
    else if(command == "get" && stream.eof())
    {
      if(queue.empty())
      {
        std::cout << "<EMPTY>" << std::endl;
      }
      else
      {
        std::cout << queue.GetElement() << std::endl;
      }
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }

    if(std::cin.eof())
    {
      break;
    }
  }
}




