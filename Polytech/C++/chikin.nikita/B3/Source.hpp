#ifndef SOURCE_HPP_
#define SOURCE_HPP_
#include <list>
#include <stdexcept>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;


class Queue
{
public:
  Queue()
  {
    std::string dum;
    highPriority = queue_.insert(queue_.end(), dum);
    normalPriority = queue_.insert(queue_.end(), dum);
  }

  void PutElement(const std::string &element, ElementPriority priority)
  {
    switch(priority)
    {
      case ElementPriority::HIGH:
      queue_.insert(highPriority, element);
      break;
      case ElementPriority::NORMAL:
      queue_.insert(normalPriority, element);
      break;
      case ElementPriority::LOW:
      queue_.insert(queue_.end(), element);
      break;
    }
  }
  std::string GetElement()
  {
    if(empty())
    {
      throw std::invalid_argument("Queue is empty!");
    }
    if(queue_.begin() != highPriority)
    {
      std::string frontElem = queue_.front();
      queue_.pop_front();
      return frontElem;
    }
    else if(std::next(highPriority) != normalPriority)
    {
      std::string frontElem = *std::next(highPriority);
      queue_.erase(std::next(highPriority));
      return frontElem;
    }
    else
    {
      std::string frontElem = *std::next(normalPriority);
      queue_.erase(std::next(normalPriority));
      return frontElem;
    }
  }
  void Accelerate()
  {
    auto it = std::next(normalPriority);
    while(it != queue_.end())
    {
      queue_.insert(highPriority, *it);
      it++;
      queue_.erase(std::prev(it));
    }
  }
  bool empty() const
  {
    if((queue_.begin() == highPriority) && (std::next(highPriority) == normalPriority)
      && (std::next(normalPriority) == queue_.end()))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
private:
  std::list<std::string> queue_;
  typename std::list<std::string>::iterator highPriority, normalPriority;
};
#endif
