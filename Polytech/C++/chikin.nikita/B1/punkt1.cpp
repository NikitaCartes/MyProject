#include <iostream>
#include "source.hpp"
#include "punkts.hpp"
#include <cstring>
#include <vector>
#include <forward_list>

void punkt1(const char *direction)
{
  std::vector<int> vector1;
  int i = 0;
  bool (*comptr)(const int&,const int&) = nullptr;
  while (std::cin >> i)
    {
      vector1.push_back(i);
    }

    if (!std::cin.eof() && std::cin.fail())
    throw std::invalid_argument("incorrect data");
  std::vector<int> vector2 = vector1;
  std::forward_list<int> list(vector1.begin(),vector1.end());
  if (std::strcmp(direction,"ascending") == 0)
  {
    comptr = more_comp;
  }
  else if (std::strcmp(direction,"descending") == 0)
  {
    comptr = less_comp;
  }
  else
  throw std::invalid_argument("nepravilno");

  sort <int,std::vector,access_by_scope<int,std::vector> >(vector1,comptr);
  Container_output <std::vector<int>> (vector1);

  sort <int,std::vector,access_by_vector<int,std::vector> >(vector2,comptr);
  Container_output <std::vector<int>> (vector1);

  sort <int,std::forward_list,access_by_iterator<int,std::forward_list> >(list,comptr);
  Container_output <std::forward_list<int>> (list);
}
