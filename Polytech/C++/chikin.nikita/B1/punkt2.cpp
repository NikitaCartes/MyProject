#include <iostream>
#include <vector>
#include <fstream>
#include <memory>

void punkt2(const char* name)
{
  std::ifstream file(name, std::ios_base::binary);

  if(file.good() == 0)
  {
    throw std::invalid_argument("Incorrect name of file");
  }
  if(!file)
    {
      throw std::ios_base::failure("Error with reading ");
    }

  file.seekg(0, std::ios_base::end);
  size_t size = file.tellg();
  file.seekg(0, std::ios_base::beg);
  if (size == 0)
   {
     exit(0);
   }
  std::unique_ptr<char[]> data(new char[size]);
  file.read(data.get(), size);
  file.close();
  std::vector<char>vector (data.get(), data.get() + size);
  for (size_t i = 0; i < vector.size(); i++)
  {
    std::cout << vector[i];
  }
}
