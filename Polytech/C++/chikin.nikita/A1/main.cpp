#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"

#include <string>

void printInformation(rectangle_t frameRect)
{
  std::cout << "x = " << frameRect.pos.x << "\ny = " << frameRect.pos.y <<
    "\nwidth = " << frameRect.width << "\nheight = " << frameRect.height << "\n";
}

int main()
{
  std::cout << "Rectangle:" << std::endl;
  double x, y, height, wight;
  wight = 10;
  height = 15;
  x = 10;
  y = 10;
  std::cout << "Set parameteres:" << std::endl;
  std::cout << "wight = " << wight << "\nheight = " << height
    << "\nx = " << x << "\ny = " << y << std::endl;
  point_t pointRectangle{ x, y };
  Rectangle rectangle{ wight, height, pointRectangle };
  rectangle.printInformation();
  std::cout << "Area = " << rectangle.getArea() << std::endl;
  std::cout << "Info about frame rect:" << std::endl;
  printInformation(rectangle.getFrameRect());
  std::string dx = "+5";
  std::string dy = "-5";
  std::cout << "Set parameteres for move:" << std::endl;
  std::cout << "dx = " << dx << "\ndy = " << dy << std::endl;
  rectangle.move(dx, dy);
  rectangle.printInformation();
  std::cout << "Info about frame rect:" << std::endl;
  printInformation(rectangle.getFrameRect());

  std::cout << "\n\nCircle:" << std::endl;
  height = 10;
  x = 5;
  y = 5;
  std::cout << "Set parameteres:" << std::endl;
  std::cout << "radius = " << height
    << "\nx = " << x << "\ny = " << y << std::endl;
  point_t pointCircle{ x, y };
  Circle circle{ height, pointCircle };
  circle.printInformation();
  std::cout << "Area = " << circle.getArea() << std::endl;
  std::cout << "Info about frame rect:" << std::endl;
  printInformation(circle.getFrameRect());
  dx = "15";
  dy = "+5";
  std::cout << "Set parameteres for move:" << std::endl;
  std::cout << "x = " << dx << "\ndy = " << dy << std::endl;
  circle.move(dx, dy);
  circle.printInformation();
  std::cout << "Info about frame rect:" << std::endl;
  printInformation(circle.getFrameRect());

  std::cout << "\n\nTriangle:" << std::endl;
  double A_x, A_y, B_x, B_y, C_x, C_y;
  A_x = 0;
  A_y = 0;
  B_x = 10;
  B_y = 10;
  C_x = 20;
  C_y = 0;
  std::cout << "Set parameteres:" << std::endl;
  std::cout << "A_x = " << A_x << "\nA_y = " << A_y
    << "\nB_x = " << B_x << "\nB_y = " << B_y
    << "\nC_x = " << C_x << "\nC_y = " << C_y << std::endl;
  point_t pointA{ A_x, A_y }, pointB{ B_x, B_y }, pointC{ C_x, C_y };
  Triangle triangle{ pointA, pointB, pointC };
  triangle.printInformation();
  std::cout << "Area = " << triangle.getArea() << std::endl;
  std::cout << "Info about frame rect:" << std::endl;
  printInformation(triangle.getFrameRect());
  dx = "20";
  dy = "20";
  std::cout << "Set parameteres for move:" << std::endl;
  std::cout << "x = " << dx << "\ny = " << dy << std::endl;
  triangle.move(dx, dy);
  triangle.printInformation();
  std::cout << "Info about frame rect:" << std::endl;
  printInformation(triangle.getFrameRect());
  return 0;
}
