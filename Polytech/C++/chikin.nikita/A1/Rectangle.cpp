#include "Rectangle.hpp"

Rectangle::Rectangle(double width, double height, point_t pos)
{
  height_ = height;
  width_ = width;
  pos_ = pos;
}
void Rectangle::printInformation() const
{
  std::cout << "Info about rectangle:" << std::endl;
  std::cout << "centre.x = " << pos_.x << "\ncentre.y = " << pos_.y <<
    "\nheight = " << height_ << "\nwigth = " << width_ << "\n";
}
double Rectangle::getArea() const
{
  return (height_ * width_);
}
rectangle_t Rectangle::getFrameRect() const
{
  rectangle_t rectangle;
  rectangle.height = height_;
  rectangle.width = width_;
  rectangle.pos.x = pos_.x;
  rectangle.pos.y = pos_.y;
  return rectangle;
}
