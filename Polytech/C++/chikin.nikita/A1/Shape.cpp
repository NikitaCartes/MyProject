#include "Shape.hpp"

void Shape::move(std::string dx, std::string dy)
{
  if (dx[0] == '+' || dx[0] == '-')
  {
    pos_.x += atof(dx.c_str());
  }
  else
  {
    pos_.x = atof(dx.c_str());
  }
  if (dy[0] == '+' || dy[0] == '-')
  {
    pos_.y += atof(dy.c_str());
  }
  else
  {
    pos_.y = atof(dy.c_str());
  }
}
