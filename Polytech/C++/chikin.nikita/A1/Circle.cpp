#include "Circle.hpp"
#define _USE_MATH_DEFINES
#include <math.h>


Circle::Circle(double radius, point_t pos)
{
  radius_ = radius;
  pos_ = pos;
}
double Circle::getArea() const
{
  return (pow(radius_, 2)*M_PI/4);
}
void Circle::printInformation() const
{
  std::cout << "Info about circle:" << std::endl;
  std::cout << "centre.x = " << pos_.x << "\ncentre.y = " << pos_.y <<
    "\nradius = " << radius_ << "\n";
}
rectangle_t Circle::getFrameRect() const
{
  rectangle_t rectangle;
  rectangle.height = radius_;
  rectangle.width = radius_;
  rectangle.pos.x = pos_.x;
  rectangle.pos.y = pos_.y;
  return rectangle;
}
