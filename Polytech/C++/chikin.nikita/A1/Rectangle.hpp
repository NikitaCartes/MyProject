#include "Shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(double, double, point_t);
  double getArea() const;
  void printInformation() const;
  rectangle_t getFrameRect() const;
protected:
  double width_;
  double height_;
};
