#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <iostream>

class Shape
{
public:
  virtual double getArea() const = 0;
  virtual rectangle_t getFrameRect() const = 0;
  virtual void move(std::string, std::string);
  virtual void printInformation() const = 0;
protected:
  point_t pos_;
};

#endif
