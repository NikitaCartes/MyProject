#include "Shape.hpp"

class Circle : public Shape
{
public:
  Circle(double, point_t);
  double getArea() const;
  void printInformation() const;
  rectangle_t getFrameRect() const;
private:
  double radius_;
};
