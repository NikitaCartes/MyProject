#include "Triangle.hpp"
#include <algorithm>


Triangle::Triangle(point_t A, point_t B, point_t C)
{
  pointA_ = A;
  pointB_ = B;
  pointC_ = C;
  pos_.x = (pointA_.x + pointB_.x + pointC_.x) / 3;
  pos_.y = (pointA_.y + pointB_.y + pointC_.y) / 3;
}
void Triangle::printInformation() const
{
  std::cout << "Info about triangle:" << std::endl;
  std::cout << "centre.x = " << pos_.x << "\ncentre.y = " << pos_.y <<
    "\nA.x = " << pointA_.x << "\nA.y = " << pointA_.y <<
    "\nB.x = " << pointB_.x << "\nB.y = " << pointB_.y <<
    "\nC.x = " << pointC_.x << "\nC.y = " << pointC_.y << "\n";
}
double Triangle::getArea() const
{
  return (abs(((pointA_.x - pointC_.x) * (pointB_.y - pointC_.y)) - ((pointA_.y - pointC_.y) * (pointB_.x - pointC_.x))) / 2);
}
rectangle_t Triangle::getFrameRect() const
{

  rectangle_t rectangle;
  double cordX[] = { pointA_.x, pointB_.x, pointC_.x };
  double cordY[] = { pointA_.y, pointB_.y, pointC_.y };
  double max_X = *std::max_element(cordX, cordX + 3);
  double min_X = *std::min_element(cordX, cordX + 3);
  double max_Y = *std::max_element(cordY, cordY + 3);
  double min_Y = *std::min_element(cordY, cordY + 3);
  rectangle.pos.x = (max_X + min_X) / 2;
  rectangle.pos.y = (max_Y + min_Y) / 2;
  rectangle.height = (rectangle.pos.y - min_Y);
  rectangle.width = (rectangle.pos.x - min_X);
  return rectangle;
}
void Triangle::move(std::string dx, std::string dy)
{
  double x = atof(dx.c_str());
  double y = atof(dy.c_str());
  if (dx[0] == '+' || dx[0] == '-')
  {
    pos_.x += x;
    pointA_.x += x;
    pointB_.x += x;
    pointC_.x += x;
  }
  else
  {
    pointA_.x += x - pos_.x;
    pointB_.x += x - pos_.x;
    pointC_.x += x - pos_.x;
    pos_.x = x;
  }
  if (dy[0] == '+' || dy[0] == '-')
  {
    pos_.y += y;
    pointA_.y += y;
    pointB_.y += y;
    pointC_.y += y;
  }
  else
  {
    pointA_.y += y - pos_.y;
    pointB_.y += y - pos_.y;
    pointC_.y += y - pos_.y;
    pos_.y = y;
  }
}
