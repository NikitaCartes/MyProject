#include "Shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(point_t, point_t, point_t);
  double getArea() const;
  void printInformation() const;
  rectangle_t getFrameRect() const;
  void move(std::string, std::string);
protected:
  point_t pointA_;
  point_t pointB_;
  point_t pointC_;
};
