#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include <iostream>

void printInformation(rectangle_t &frameRect)
{
  std::cout << "x = " << frameRect.pos.x << "\ny = " << frameRect.pos.y <<
    "\nwidth = " << frameRect.width << "\nheight = " << frameRect.height << "\n";
}

int main()
{
  std::cout << "Rectangle:" << std::endl;
  Rectangle rectangle{ {5, 2.5}, {5, 17.5}, {15, 17.5}, {15, 2.5} };
  rectangle.printInformation();
  std::cout << "Area = " << rectangle.getArea() << std::endl;
  rectangle.scale(2);
  std::cout << "Scaling coefficient = 2" << std::endl;
  std::cout << "Area after scale = " << rectangle.getArea() << std::endl;
  rectangle.printInformation();
  std::cout << "\n\nCircle:" << std::endl;
  Circle circle{ { 5, 5 }, 10};
  circle.printInformation();
  std::cout << "Area = " << circle.getArea() << std::endl;
  circle.scale(0.5);
  std::cout << "Scaling coefficient = 0.5" << std::endl;
  std::cout << "Area after scale = " << circle.getArea() << std::endl;
  circle.printInformation();
  std::cout << "\n\nTriangle:" << std::endl;
  Triangle triangle{ { 0,0 },{ 10,10 },{ 20, 0 } };
  triangle.printInformation();
  std::cout << "Area = " << triangle.getArea() << std::endl;
  triangle.scale(3);
  std::cout << "Scaling coefficient = 3" << std::endl;
  std::cout << "Area after scale = " << triangle.getArea() << std::endl;
  triangle.printInformation();
  return 0;
}
