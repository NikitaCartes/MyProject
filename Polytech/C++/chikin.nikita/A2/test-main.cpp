#ifndef BOOST_TEST_MAIN
#define BOOST_TEST_MAIN

#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>

BOOST_AUTO_TEST_SUITE(Rectangle_tests)

  BOOST_AUTO_TEST_CASE(Rectangle_area_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    BOOST_REQUIRE_EQUAL(rect.getArea(), 150);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_move_cord_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rect.move(15, -10);
    BOOST_REQUIRE_EQUAL(rect.getArea(), 150);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_set_cord_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rect.move({ 15, 10 });
    BOOST_REQUIRE_EQUAL(rect.getArea(), 150);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_frame_area_move_cord_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rectangle_t rectangle = rect.getFrameRect();
    const double wigth = rectangle.width;
    const double height = rectangle.height;
    const point_t pos = rectangle.pos;
    rect.move(15, -10);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().width, wigth);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().height, height);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().pos.x, pos.x + 15);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().pos.y, pos.y - 10);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_frame_area_set_cord_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rectangle_t rectangle = rect.getFrameRect();
    const double wigth = rectangle.width;
    const double height = rectangle.height;
    rect.move({15, 10});
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().width, wigth);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().height, height);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().pos.x, 15);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().pos.y, 10);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_scale_up_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const double area = rect.getArea();
    rect.scale(4);
    BOOST_REQUIRE_EQUAL(rect.getArea(), area * 16);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_scale_down_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const double area = rect.getArea();
    rect.scale(0.1);
    BOOST_CHECK_CLOSE_FRACTION(rect.getArea(), area/100, 0.001);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_invalid_height_test)
  {
    BOOST_CHECK_THROW(Rectangle({ 5, 2.5 }, { 5, 2.5 }, { 15, 17.5 }, { 15, 2.5 }), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_invalid_wigth_test)
  {
    BOOST_CHECK_THROW(Rectangle({ 5, 2.5 }, { 15, 17.5 }, { 15, 17.5 }, { 15, 2.5 }), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_not_rectangle_test)
  {
    BOOST_CHECK_THROW(Rectangle({ 0, 0 }, { 0, 10 }, { 5, 7.5 }, { 5, 2.5 }), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_invalid_scale_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    BOOST_CHECK_THROW(rect.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Circle_tests)

  BOOST_AUTO_TEST_CASE(Circle_area_test)
  {
    Circle circle{ { 10,10 }, 10 };
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), pow(10, 2)*M_PI, 0.001);
  }

  BOOST_AUTO_TEST_CASE(Circle_area_move_cord_test)
  {
    Circle circle{ { 10,10 }, 10 };
    circle.move(15, -10);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), pow(10, 2)*M_PI, 0.001);
  }

  BOOST_AUTO_TEST_CASE(Circle_area_set_cord_test)
  {
    Circle circle{ { 10,10 }, 10 };
    circle.move({ 15, 10 });
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), pow(10, 2)*M_PI, 0.001);
  }

  BOOST_AUTO_TEST_CASE(Circle_frame_area_move_cord_test)
  {
    Circle circle{ { 10,10 }, 10 };
    rectangle_t rectangle = circle.getFrameRect();
    const double wigth = rectangle.width;
    const double height = rectangle.height;
    const point_t pos = rectangle.pos;
    circle.move(15, -10);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().width, wigth);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().height, height);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().pos.x, pos.x + 15);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().pos.y, pos.y - 10);
  }

  BOOST_AUTO_TEST_CASE(Circle_frame_area_set_cord_test)
  {
    Circle circle{ { 10,10 }, 10 };
    rectangle_t rectangle = circle.getFrameRect();
    const double wigth = rectangle.width;
    const double height = rectangle.height;
    circle.move({ 15, 10 });
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().width, wigth);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().height, height);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().pos.x, 15);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().pos.y, 10);
  }

  BOOST_AUTO_TEST_CASE(Circle_scale_up_test)
  {
    Circle circle{ { 10,10 }, 10 };
    const double area = circle.getArea();
    circle.scale(4);
    BOOST_REQUIRE_EQUAL(circle.getArea(), area * 16);
  }

  BOOST_AUTO_TEST_CASE(Circle_scale_down_test)
  {
    Circle circle{ { 10,10 }, 10 };
    const double area = circle.getArea();
    circle.scale(0.1);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), area / 100, 0.001);
  }

  BOOST_AUTO_TEST_CASE(Circle_invalid_radius_test)
  {
    BOOST_CHECK_THROW(Circle({ 10,10 }, -10), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Circle_invalid_scale_test)
  {
    Circle circle{ { 10,10 }, 10 };
    BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Triangle_tests)

  BOOST_AUTO_TEST_CASE(Triangle_area_test)
  {
    Triangle triangle{{0,0},{10,10},{20,2}};
    BOOST_REQUIRE_EQUAL(triangle.getArea(), 90);
  }

  BOOST_AUTO_TEST_CASE(Triangle_area_move_cord_test)
  {
    Triangle triangle{{0,0},{10,10},{20,2}};
    triangle.move(15, -10);
    BOOST_REQUIRE_EQUAL(triangle.getArea(), 90);
  }

  BOOST_AUTO_TEST_CASE(Triangle_area_set_cord_test)
  {
    Triangle triangle{{0,0},{10,10},{20,2}};
    triangle.move({ 15, 10 });
    BOOST_REQUIRE_EQUAL(triangle.getArea(), 90);
  }

  BOOST_AUTO_TEST_CASE(Triangle_frame_area_move_cord_test)
  {
    Triangle triangle{{0,0},{10,10},{20,2}};
    rectangle_t rectangle = triangle.getFrameRect();
    const double wigth = rectangle.width;
    const double height = rectangle.height;
    triangle.move(15, -10);
    BOOST_REQUIRE_EQUAL(triangle.getFrameRect().width, wigth);
    BOOST_REQUIRE_EQUAL(triangle.getFrameRect().height, height);
  }

  BOOST_AUTO_TEST_CASE(Triangle_frame_area_set_cord_test)
  {
    Triangle triangle{{0,0},{10,10},{20,2}};
    rectangle_t rectangle = triangle.getFrameRect();
    const double wigth = rectangle.width;
    const double height = rectangle.height;
    triangle.move({ 15, 10 });
    BOOST_REQUIRE_EQUAL(triangle.getFrameRect().width, wigth);
    BOOST_REQUIRE_EQUAL(triangle.getFrameRect().height, height);
  }

  BOOST_AUTO_TEST_CASE(Triangle_scale_up_test)
  {
    Triangle triangle{{0,0},{10,10},{20,2}};
    const double area = triangle.getArea();
    triangle.scale(4);
    BOOST_REQUIRE_EQUAL(triangle.getArea(), area * 16);
  }

  BOOST_AUTO_TEST_CASE(Triangle_scale_down_test)
  {
    Triangle triangle{{0,0},{10,10},{20,0}};
    const double area = triangle.getArea();
    triangle.scale(0.1);
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), area / 100, 0.001);
  }

  BOOST_AUTO_TEST_CASE(Triangle_invalid_scale_test)
  {
    Triangle triangle{{0,0},{10,10},{20,2}};
    BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

#endif
