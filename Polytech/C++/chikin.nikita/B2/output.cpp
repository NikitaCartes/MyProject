#include "edit.hpp"

void Print (const std::vector<element_t> &line)
{
  for (auto it = line.begin(); it != line.end(); it++)
  {
    std::cout << (*it).value;
  }
  std::cout << "\n";
}

int Reorganize (std::vector<element_t> &line)
{
  std::size_t width = 0;
  std::vector<element_t>newLine;
  while (!line.empty())
  {
    newLine.insert(newLine.begin(),line.back());
    width += line.back().value.size();
    line.pop_back();
    if (newLine.front().type == element_t::WORD || newLine.front().type == element_t::NUMBER)
    {
      break;
    }
  }
  Print(line);
  line = newLine;
  return width;
}

void Formatting (unsigned int width, const std::vector<element_t> &vector)
{
  std::size_t currentWidth = 0;
  std::vector<element_t> line;
  for(auto it = vector.begin(); it != vector.end(); it++)
  {
    switch((*it).type)
    {
    case element_t::MARK:
      if(currentWidth + 1 > width)
      {
        currentWidth = Reorganize(line);
      }
      line.push_back(*it);
      currentWidth += (*it).value.size();
      break;

    case element_t::DASH:
      if(currentWidth + 4 > width)
      {
        currentWidth = Reorganize(line);
      }
      line.push_back(element_t { " ", element_t::SPACE });
      line.push_back(*it);
      currentWidth += (*it).value.size() + 1;
      break;

    case element_t::WORD:
    case element_t::NUMBER:
      if(currentWidth + (*it).value.size() + 1 > width)
      {
        Print(line);
        line.clear();
        currentWidth = 0;
      }
      else if(!line.empty())
      {
        line.push_back(element_t { " ", element_t::SPACE });
        currentWidth++;
      }
      line.push_back(*it);
      currentWidth += (*it).value.size();
      break;
    case element_t::SPACE:
      break;
    }

  }
  if(!line.empty())
  {
    Print(line);
  }
}
