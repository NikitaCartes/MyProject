#include "edit.hpp"

int main(int args, char * argv[])
{
  int width = 40;
  if (args > 1)
  {
    if (args != 3)
    {
      std::cerr << "Invalid number of arguments.";
      return 1;
    }
    else if (std::string(argv[1]) != "--line-width")
    {
      std::cerr << "Invalid arguments.";
      return 1;
    }
    else
    {
      width = std::stoi(argv[2]);
      if (width < 24)
      {
        std::cerr << "Invalid line width.";
        return 1;
      }
    }
  }
  Input text;
  text.Read();
  if (!Error(text.vector))
  {
    std::cerr << "Invalid input.";
    return 1;
  }
  Formatting(width, text.vector);
  return 0;
}




