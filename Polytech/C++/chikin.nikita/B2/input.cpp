#include "edit.hpp"

void Input::Word()
{
  element_t el { "", element_t::WORD };
  do
  {
    char c = std::cin.get();
    el.value.push_back(c);

    if(c == '-' && std::cin.peek() == '-')
    {
      el.value.pop_back();
      std::cin.unget();
      break;
    }
  } while ((std::isalpha<char>(std::cin.peek(), std::locale())) || (std::cin.peek() == '-'));
  vector.push_back(el);
}

void Input::Number()
{
  element_t el { "", element_t::NUMBER };
  char point = std::use_facet<std::numpunct<char>>(std::locale()).decimal_point();
  bool pointRead = false;
  do
  {
    char c = std::cin.get();
    if(c == point)
    {
      if(pointRead)
      {
        std::cin.unget();
        break;
      }
      pointRead = true;
    }
    el.value.push_back(c);
  } while((std::isdigit<char>(std::cin.peek(), std::locale()) || (std::cin.peek() == point)));

  vector.push_back(el);
}

void Input::Dash()
{
  element_t el{ "", element_t::DASH };
  while(std::cin.peek() == '-')
  {
    char c = std::cin.get();
    el.value.push_back(c);
  }
  vector.push_back(el);
}

void Input::Element()
{
  char c = std::cin.get();
  while(std::isspace(c, std::locale()))
  {
    c = std::cin.get();
  }

  if (isalpha(c, std::locale()))
  {
    std::cin.unget();
    Word();
  }
  else if (c == '-')
  {
    if (std::cin.peek() == '-')
    {
      std::cin.unget();
      Dash();
    }
    else
    {
      std::cin.unget();
      Number();
    }
  }
  else if ((c == '+' || (isdigit(c, std::locale()))))
  {
    std::cin.unget();
    Number();
  }
  else if (ispunct(c, std::locale()))
  {
    element_t el{ "", element_t::MARK };
    el.value.push_back(c);
    vector.push_back(el);
  }
}

void Input::Read()
{
  while(std::cin)
  {
    Element();
  }
}
