#ifndef EDIT_HPP_
#define EDIT_HPP_
#include <iostream>
#include <string>
#include <vector>
#include <memory>

struct element_t
{
  enum type_t
  {
    NUMBER, WORD, MARK, DASH, SPACE
  };

  std::string value;
  type_t type;
};

struct Input
{
  void Read();
  void Word();
  void Number();
  void Dash();
  void Element();
  std::vector<element_t> vector;
};

void Formatting(unsigned int width, const std::vector<element_t> &vector);
bool Error (const std::vector<element_t> &vector);

#endif /* EDIT_HPP_ */
