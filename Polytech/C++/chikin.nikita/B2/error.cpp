#include "edit.hpp"

bool Error(const std::vector<element_t> &vector)
{
  if(!vector.empty() && (vector.front().type != element_t::WORD) && (vector.front().type != element_t::NUMBER))
  {
    return false;
  }
  for(auto it = vector.begin(); it != vector.end(); it++)
  {
    switch((*it).type)
    {
    case element_t::WORD:
    case element_t::NUMBER:
      if((*it).value.size() > 20)
      {
        return false;
      }
      break;
    case element_t::DASH:
      if((*it).value.size() != 3)
      {
        return false;
      }
      if(it != vector.begin())
      {
        const element_t &prev = *std::prev(it);
        if((prev.type == element_t::DASH) || ((prev.type == element_t::MARK) && (prev.value != ",")))
        {
          return false;
        }
      }
      break;
    case element_t::MARK:
      if(it != vector.begin())
      {
        const element_t &prev = *std::prev(it);
        if((prev.type == element_t::DASH) || (prev.type == element_t::MARK))
        {
          return false;
        }
      }
      break;
    case element_t::SPACE:
      break;
    }
  }
  return true;
}





