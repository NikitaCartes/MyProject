#ifndef BOOST_TEST_MAIN
#define BOOST_TEST_MAIN

#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include "CompositeShape.hpp"
#include "Polygon.hpp"
#include "Matrix.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>

BOOST_AUTO_TEST_SUITE(Constructors_tests)

  BOOST_AUTO_TEST_CASE(Circle_invalid_radius_test)
  {
    BOOST_CHECK_THROW(Circle({ 10,10 }, -10), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Triangle_invalid_points_test)
  {
    BOOST_CHECK_THROW(Triangle( { 0,0 },{ 0,5 },{ 0, 10 } ), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_invalid_points_test)
  {
    BOOST_CHECK_THROW(Rectangle({ 0,0 }, { 0,5 }, { 0, 10 }, { 0, 15 }), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_invalid_width_test)
  {
    BOOST_CHECK_THROW(Rectangle(0, 10, {0, 0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_invalid_height_test)
  {
    BOOST_CHECK_THROW(Rectangle(10, 0, { 0, 0 }), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_not_rectangle_test)
  {
    BOOST_CHECK_THROW(Rectangle({ 0, 0 }, { 0, 10 }, { 5, 7.5 }, { 5, 2.5 }), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_new_null)
  {
    BOOST_CHECK_THROW(CompositeShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Polygon_new_null)
  {
    BOOST_CHECK_THROW(Polygon(nullptr, 5); , std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Polygon_new_less_3)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0)} };
    BOOST_CHECK_THROW(Polygon(points, 2); , std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Polygon_new_area_zero)
  {
    point_t points[] = { { 0,0 },{ 10, 0},{ 20, 0 },{ 30, 0 } };
    BOOST_CHECK_THROW(Polygon(points, 4);, std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(getArea_tests)

  BOOST_AUTO_TEST_CASE(Circle_area_test)
  {
    Circle circle{ { 10,10 }, 10 };
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), pow(10, 2)*M_PI, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), 150, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{0, 0} };
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), 150, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Triangle_area_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), 90, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_test)
  {
    double area = 0;
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    area += rectangle.getArea();
    area += triangle.getArea();
    area += circle.getArea();
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Polygon_area_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
      { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 }, {1,0} };
    Polygon polygon(points, 6);
    BOOST_CHECK_CLOSE_FRACTION(polygon.getArea(), 2.598, 0.1);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(getCentre_tests)

  BOOST_AUTO_TEST_CASE(Circle_centre_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    const point_t temp{ 5, 5 };
    BOOST_CHECK(circle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const point_t temp{ 10, 10 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    const point_t temp{ 0, 0 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_centre_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    const point_t temp{ 10, 4 };
    BOOST_CHECK(triangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_centre_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const point_t temp{ 7.5, 6.25 };
    BOOST_CHECK(composite.getCentre() == temp);

  }

  BOOST_AUTO_TEST_CASE(Polygon_centre_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    const point_t temp{ 0.5, 0.866 };
    BOOST_CHECK(polygon.getCentre() == temp);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(getFrameRect_tests)

  BOOST_AUTO_TEST_CASE(Circle_getFrameRect_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    const rectangle_t temp{ 20, 20,{ 5, 5 } };
    BOOST_CHECK(circle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const rectangle_t temp{ 10, 15,{ 10, 10 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    const rectangle_t temp{ 10, 15,{ 0, 0 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_getFrameRect_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    const rectangle_t temp{ 20, 10,{ 10, 5 } };
    BOOST_CHECK(triangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_getFrameRect_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const rectangle_t temp{ 25, 22.5,{ 7.5, 6.25 } };
    BOOST_CHECK(composite.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Polygon_getFrameRect_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    const rectangle_t temp{ 2, sqrt(3),{ 0.5, 0.866 } };
    BOOST_CHECK(polygon.getFrameRect() == temp);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(move_to_tests)

  BOOST_AUTO_TEST_CASE(Circle_centre_after_move_to_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    circle.move({20, 20});
    const point_t temp{ 20, 20 };
    BOOST_CHECK(circle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_after_move_to_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rectangle.move({ 20, 20 });
    const point_t temp{ 20, 20 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_after_move_to_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    rectangle.move({ 20, 20 });
    const point_t temp{ 20, 20 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_centre_after_move_to_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    triangle.move({ 20, 20 });
    const point_t temp{ 20, 20 };
    BOOST_CHECK(triangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_centre_after_move_to_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.move({ 20, 20 });
    const point_t temp{ 20, 20 };
    BOOST_CHECK(composite.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Polygon_centre_after_move_to_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    polygon.move({ 20, 20 });
    const point_t temp{ 20, 20 };
    BOOST_CHECK(polygon.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Circle_area_after_move_to_test)
  {
    Circle circle{ { 10,10 }, 10 };
    const double area = circle.getArea();
    circle.move({ 20, 20 });
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_move_to_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const double area = rectangle.getArea();
    rectangle.move({ 20, 20 });
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_move_to_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    const double area = rectangle.getArea();
    rectangle.move({ 20, 20 });
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Triangle_area_after_move_to_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    const double area = triangle.getArea();
    triangle.move({ 20, 20 });
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_after_move_to_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const double area = composite.getArea();
    composite.move({ 20, 20 });
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Polygon_area_after_move_to_test_2)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    const double area = polygon.getArea();
    polygon.move({ 20, 20 });
    BOOST_CHECK_CLOSE_FRACTION(polygon.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Circle_getFrameRect_after_move_to_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    circle.move({ 20, 20 });
    const rectangle_t temp{ 20, 20,{ 20, 20 } };
    BOOST_CHECK(circle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_after_move_to_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rectangle.move({ 20, 20 });
    const rectangle_t temp{ 10, 15,{ 20, 20 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_after_move_to_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    rectangle.move({ 20, 20 });
    const rectangle_t temp{ 10, 15,{ 20, 20 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_getFrameRect_after_move_to_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    triangle.move({ 20, 20 });
    const rectangle_t temp{ 20, 10,{ 20, 21 } };
    BOOST_CHECK(triangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_getFrameRect_after_move_to_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.move({ 20, 20 });
    const rectangle_t temp{ 25, 22.5,{ 20, 20 } };
    BOOST_CHECK(composite.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Polygon_getFrameRect_after_move_to_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    polygon.move({ 20, 20 });
    const rectangle_t temp{ 2 , sqrt(3),{ 20, 20 } };
    BOOST_CHECK(polygon.getFrameRect() == temp);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(move_on_tests)

  BOOST_AUTO_TEST_CASE(Circle_centre_after_move_on_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    circle.move(20, 20);
    const point_t temp{ 25, 25 };
    BOOST_CHECK(circle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_after_move_on_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rectangle.move(20, 20);
    const point_t temp{ 30, 30 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_after_move_on_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    rectangle.move(20, 20);
    const point_t temp{ 20, 20 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_centre_after_move_on_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    triangle.move(20, 20);
    const point_t temp{ 30, 24 };
    BOOST_CHECK(triangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_centre_after_move_on_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.move(20, 20);
    const point_t temp{ 27.5, 26.25 };
    BOOST_CHECK(composite.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Polygon_centre_after_move_on_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    polygon.move(20, 20);
    const point_t temp{ 20.5, 20.866 };
    BOOST_CHECK(polygon.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Circle_area_after_move_on_test)
  {
    Circle circle{ { 10,10 }, 10 };
    const double area = circle.getArea();
    circle.move(20, 20);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_move_on_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const double area = rectangle.getArea();
    rectangle.move(20, 20);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_move_on_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    const double area = rectangle.getArea();
    rectangle.move(20, 20);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Triangle_area_after_move_on_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    const double area = triangle.getArea();
    triangle.move(20, 20);
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_after_move_on_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const double area = composite.getArea();
    composite.move(20, 20);
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Polygon_area_after_move_on_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    const double area = polygon.getArea();
    polygon.move(20, 20);
    BOOST_CHECK_CLOSE_FRACTION(polygon.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Circle_getFrameRect_after_move_on_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    circle.move(20, 20);
    const rectangle_t temp{ 20, 20,{ 25, 25 } };
    BOOST_CHECK(circle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_after_move_on_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rectangle.move(20, 20);
    const rectangle_t temp{ 10, 15,{ 30, 30 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_after_move_on_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    rectangle.move(20, 20);
    const rectangle_t temp{ 10, 15,{ 20, 20 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_getFrameRect_after_move_on_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    triangle.move(20, 20);
    const rectangle_t temp{ 20, 10,{ 30, 25 } };
    BOOST_CHECK(triangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_getFrameRect_after_move_on_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.move(20, 20);
    const rectangle_t temp{ 25, 22.5,{ 27.5, 26.25 } };
    BOOST_CHECK(composite.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Polygon_getFrameRect_after_move_on_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    polygon.move(20, 20);
    const rectangle_t temp{ 2 , sqrt(3),{ 20.5, 20.866 } };
    BOOST_CHECK(polygon.getFrameRect() == temp);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(scale_tests)

  BOOST_AUTO_TEST_CASE(Circle_area_after_scale_up_test)
  {
    Circle circle{ { 10,10 }, 10 };
    const double area = circle.getArea();
    circle.scale(10);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), area*100, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_scale_up_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const double area = rectangle.getArea();
    rectangle.scale(10);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area*100, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_scale_up_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    const double area = rectangle.getArea();
    rectangle.scale(10);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area*100, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Triangle_area_after_scale_up_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    const double area = triangle.getArea();
    triangle.scale(10);
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), area*100, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_after_scale_up_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const double area = composite.getArea();
    composite.scale(10);
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area*100, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Polygon_area_after_scale_up_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    const double area = polygon.getArea();
    polygon.scale(10);
    BOOST_CHECK_CLOSE_FRACTION(polygon.getArea(), area * 100, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Circle_area_after_scale_down_test)
  {
    Circle circle{ { 10,10 }, 10 };
    const double area = circle.getArea();
    circle.scale(0.5);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), area * 0.25, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_scale_down_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const double area = rectangle.getArea();
    rectangle.scale(0.5);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area * 0.25, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_scale_down_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    const double area = rectangle.getArea();
    rectangle.scale(0.5);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area * 0.25, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Triangle_area_after_scale_down_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    const double area = triangle.getArea();
    triangle.scale(0.5);
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), area * 0.25, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_after_scale_down_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const double area = composite.getArea();
    composite.scale(0.5);
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area * 0.25, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Polygon_area_after_scale_down_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    const double area = polygon.getArea();
    polygon.scale(0.5);
    BOOST_CHECK_CLOSE_FRACTION(polygon.getArea(), area * 0.25, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Circle_invalid_scale_test)
  {
    Circle circle{ { 10,10 }, 10 };
    BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Triangle_invalid_scale_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_invalid_scale_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    BOOST_CHECK_THROW(rect.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_invalid_scale_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_CHECK_THROW(composite.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Polygon_invalid_scale_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    BOOST_CHECK_THROW(polygon.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(rotate_relative_centre_tests)

  BOOST_AUTO_TEST_CASE(Circle_centre_after_rotate_relative_centre_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    circle.rotate(45);
    const point_t temp{ 5, 5 };
    BOOST_CHECK(circle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_after_rotate_relative_centre_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rectangle.rotate(45);
    const point_t temp{ 10, 10 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_after_rotate_relative_centre_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 5, 5 } };
    rectangle.rotate(45);
    const point_t temp{ 5, 5 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_centre_after_rotate_relative_centre_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    triangle.rotate(45);
    const point_t temp{ 10, 4 };
    BOOST_CHECK(triangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_centre_after_rotate_relative_centre_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.rotate(45);
    const point_t temp{ 8.3336, 6.5533 };
    BOOST_CHECK(composite.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Polygon_centre_after_rotate_relative_centre_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    polygon.rotate(45);
    const point_t temp{ 0.5, 0.866 };
    BOOST_CHECK(polygon.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Circle_area_after_rotate_relative_centre_test)
  {
    Circle circle{ { 10,10 }, 10 };
    const double area = circle.getArea();
    circle.rotate(45);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_rotate_relative_centre_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const double area = rectangle.getArea();
    rectangle.rotate(45);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_rotate_relative_centre_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    const double area = rectangle.getArea();
    rectangle.rotate(45);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Triangle_area_after_rotate_relative_centre_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    const double area = triangle.getArea();
    triangle.rotate(45);
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_after_rotate_relative_centre_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const double area = composite.getArea();
    composite.rotate(45);
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Polygon_area_after_rotate_relative_centre_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    const double area = polygon.getArea();
    polygon.rotate(45);
    BOOST_CHECK_CLOSE_FRACTION(polygon.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Circle_getFrameRect_after_rotate_relative_centre_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    circle.rotate(45);
    const rectangle_t temp{ 20, 20,{ 5, 5 } };
    BOOST_CHECK(circle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_after_rotate_relative_centre_test_1)
  {
    Rectangle rectangle{ {6, 7}, {6, 13}, {14, 13}, {14, 7} };
    rectangle.rotate(45);
    const rectangle_t temp{ 9.8995, 9.8995,{ 10, 10 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_after_rotate_relative_centre_test_2)
  {
    Rectangle rectangle{ 10, 10 ,{ 10, 10 } };
    rectangle.rotate(45);
    const rectangle_t temp{ 14.1421, 14.1421,{ 10, 10 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_getFrameRect_after_rotate_relative_centre_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,0 } };
    triangle.rotate(45);
    const rectangle_t temp{ 14.1421, 14.1421,{ 12.357, 0.9763 } };
    BOOST_CHECK(triangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_getFrameRect_after_rotate_relative_centre_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.rotate(45);
    const rectangle_t temp{ 23.4350, 25.9099,{ 8.3336, 6.5533 } };
    BOOST_CHECK(composite.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Polygon_getFrameRect_after_rotate_relative_centre_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    polygon.rotate(45);
    const rectangle_t temp{ 1.9318, 1.9318,{ 0.5, 0.866 } };
    BOOST_CHECK(polygon.getFrameRect() == temp);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(rotate_relative_point_tests)

  BOOST_AUTO_TEST_CASE(Circle_centre_after_rotate_relative_point_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    circle.rotate(45, {0, 0});
    const point_t temp{ 0, 7.071 };
    BOOST_CHECK(circle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_after_rotate_relative_point_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    rectangle.rotate(45, {0, 0});
    const point_t temp{ 0, 14.1421 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_centre_after_rotate_relative_point_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 5, 5 } };
    rectangle.rotate(45, {0, 0});
    const point_t temp{ 0, 7.071 };
    BOOST_CHECK(rectangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_centre_after_rotate_relative_point_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    triangle.rotate(45, {0, 0});
    const point_t temp{ 4.2426, 9.8994 };
    BOOST_CHECK(triangle.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_centre_after_rotate_relative_point_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.rotate(45, {0, 0});
    const point_t temp{ 1.7175, 10.0260 };
    BOOST_CHECK(composite.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Polygon_centre_after_rotate_relative_point_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    polygon.rotate(45, { 0, 0 });
    const point_t temp{ -0.2588, 0.966 };
    BOOST_CHECK(polygon.getCentre() == temp);
  }

  BOOST_AUTO_TEST_CASE(Circle_area_after_rotate_relative_point_test)
  {
    Circle circle{ { 10,10 }, 10 };
    const double area = circle.getArea();
    circle.rotate(45, {0, 0});
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_rotate_relative_point_test_1)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    const double area = rectangle.getArea();
    rectangle.rotate(45, {0, 0});
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_area_after_rotate_relative_point_test_2)
  {
    Rectangle rectangle{ 10, 15 ,{ 0, 0 } };
    const double area = rectangle.getArea();
    rectangle.rotate(45, {0, 0});
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Triangle_area_after_rotate_relative_point_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,2 } };
    const double area = triangle.getArea();
    triangle.rotate(45, {0, 0});
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_after_rotate_relative_point_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const double area = composite.getArea();
    composite.rotate(45, {0, 0});
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Polygon_area_after_rotate_relative_point_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    const double area = polygon.getArea();
    polygon.rotate(45, { 0, 0 });
    BOOST_CHECK_CLOSE_FRACTION(polygon.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(Circle_getFrameRect_after_rotate_relative_point_test)
  {
    Circle circle{ { 5, 5 }, 10 };
    circle.rotate(45, {0, 0});
    const rectangle_t temp{ 20, 20,{ 0, 7.071 } };
    BOOST_CHECK(circle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_after_rotate_relative_point_test_1)
  {
    Rectangle rectangle{ { 6, 7 },{ 6, 13 },{ 14, 13 },{ 14, 7 } };
    rectangle.rotate(45, {0, 0});
    const rectangle_t temp{ 9.8995, 9.8995,{ 0, 14.1421 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_getFrameRect_after_rotate_relative_point_test_2)
  {
    Rectangle rectangle{ 10, 10 ,{ 10, 10 } };
    rectangle.rotate(45, {0, 0});
    const rectangle_t temp{ 14.1421, 14.1421,{ 0, 14.1421 } };
    BOOST_CHECK(rectangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Triangle_getFrameRect_after_rotate_relative_point_test)
  {
    Triangle triangle{ { 0,0 },{ 10,10 },{ 20,0 } };
    triangle.rotate(45, {0, 0});
    const rectangle_t temp{ 14.1421, 14.1421,{ 7.071, 7.071 } };
    BOOST_CHECK(triangle.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_getFrameRect_after_rotate_relative_point_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.rotate(45, {0, 0});
    const rectangle_t temp{ 23.435, 25.9099,{ 1.7175, 10.026 } };
    BOOST_CHECK(composite.getFrameRect() == temp);
  }

  BOOST_AUTO_TEST_CASE(Polygon_getFrameRect_after_rotate_relative_point_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    polygon.rotate(45, { 0, 0 });
    const rectangle_t temp{ 1.9318, 1.9318,{ -0.2588, 0.966 } };
    BOOST_CHECK(polygon.getFrameRect() == temp);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShape_tests)

  BOOST_AUTO_TEST_CASE(CompositeShape_add_null)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    BOOST_CHECK_THROW(composite.addShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_length_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    BOOST_REQUIRE_EQUAL(composite.getLength(), 1);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    BOOST_REQUIRE_EQUAL(composite.getLength(), 2);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_REQUIRE_EQUAL(composite.getLength(), 3);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_invalid_brackets_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_CHECK_THROW(composite[4], std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_valid_brackets_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_CHECK(rect.getFrameRect() == composite[0].getFrameRect());
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_invalid_number_deleting_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_CHECK_THROW(composite.deleteShape(4), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_length_after_deleting_test)
  {
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.deleteShape(1);
    BOOST_REQUIRE_EQUAL(composite.getLength(), 2);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_after_deleting_test)
  {
    double area = 0;
    Rectangle rect{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.deleteShape(1);
    area += rect.getArea();
    area += triangle.getArea();
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Polygon_tests)

  BOOST_AUTO_TEST_CASE(Polygon_add_null)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    BOOST_CHECK_THROW(polygon.addVertex(nullptr, 5), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Polygon_length_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 }};
    Polygon polygon(points, 5);
    BOOST_REQUIRE_EQUAL(polygon.getLength(), 5);
    point_t point{ 1,0 };
    polygon.addVertex(point);
    BOOST_REQUIRE_EQUAL(polygon.getLength(), 6);
  }

  BOOST_AUTO_TEST_CASE(Polygon_invalid_brackets_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    BOOST_CHECK_THROW(polygon[7], std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Polygon_valid_brackets_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    point_t point = polygon[3];
    BOOST_REQUIRE_EQUAL(point.x, 1);
    BOOST_REQUIRE_EQUAL(point.y, sqrt(3));
  }

  BOOST_AUTO_TEST_CASE(Polygon_invalid_number_deleting_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    BOOST_CHECK_THROW(polygon.deleteVertex(7), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Polygon_length_after_deleting_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 } };
    Polygon polygon(points, 6);
    polygon.deleteVertex(1);
    BOOST_REQUIRE_EQUAL(polygon.getLength(), 5);
  }

  BOOST_AUTO_TEST_CASE(Polygon_area_after_deleting_test)
  {
    point_t points[] = { { 0,0 },{ -(1.0 / 2.0), sqrt(3) / 2 },{ 0, sqrt(3) },
    { 1, sqrt(3) },{ 3.0 / 2.0, sqrt(3) / 2 },{ 1,0 }, {0.5, -1} };
    Polygon polygon(points, 7);
    double area = polygon.getArea();
    polygon.deleteVertex(5);
    BOOST_CHECK_CLOSE_FRACTION(polygon.getArea(), area - 0.43, 0.1);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Matrix_tests)

  BOOST_AUTO_TEST_CASE(Matrix_add_null)
  {
    Rectangle rect{ 2, 2,{ 10, 10 } };
    Matrix matrix(&rect);
    BOOST_CHECK_THROW(matrix.addShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Matrix_brackets_invalid_test)
  {
    Rectangle rect{ 2, 2,{ 10, 10 } };
    Matrix matrix(&rect);
    BOOST_CHECK_THROW(matrix[10], std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Matrix_brackets_valid_test)
  {
    Rectangle rect{ 2, 2,{ 10, 10 } };
    Circle circle{ { 10, 10 }, 3 };
    Triangle triangle{ { 4, -4 },{ 5, 0 },{ 6, 0 } };
    Matrix matrix(&rect);
    matrix.addShape(&circle);
    matrix.addShape(&triangle);
    BOOST_CHECK(rect.getFrameRect() == matrix[0][0]->getFrameRect());
    BOOST_CHECK(circle.getFrameRect() == matrix[1][0]->getFrameRect());
    BOOST_CHECK(triangle.getFrameRect() == matrix[0][1]->getFrameRect());
  }

  BOOST_AUTO_TEST_CASE(Matrix_invalid_number_deleting_test)
  {
    Rectangle rect{ 2, 2,{ 10, 10 } };
    Circle circle{ { 0, 0 }, 3 };
    Triangle triangle{ { 4, -4 },{ 5, 0 },{ 6, 0 } };
    Matrix matrix(&rect);
    matrix.addShape(&circle);
    matrix.addShape(&triangle);
    BOOST_CHECK_THROW(matrix.deleteShape(5, 5), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

#endif
