#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include "CompositeShape.hpp"
#include "Polygon.hpp"
#include "Matrix.hpp"
#include <string>
#include <iostream>
#include <math.h>

void printInformation(rectangle_t &frameRect)
{
  std::cout << "x = " << frameRect.pos.x << "\n" <<
    "y = " << frameRect.pos.y << "\n" <<
    "width = " << frameRect.width << "\n" <<
    "height = " << frameRect.height << std::endl;
}

int main()
{
  point_t points[] = { { 0,0 }, { -(1.0 / 2.0), sqrt(3) / 2 }, { 0, sqrt(3) }, {1, sqrt(3) } };
  Polygon polygon(points, 4);
  polygon.printInformation();
  point_t point5{ 3.0 / 2.0, sqrt(3) / 2 };
  point_t point6{ 1, 0 };
  polygon.addVertex(point5);
  polygon.addVertex(point6);
  polygon.printInformation();
  std::cout << "Area = " << polygon.getArea() << std::endl;
  polygon.move({ 0, 0 });
  polygon.scale(2);
  polygon.printInformation();
  std::cout << "Area = " << polygon.getArea() << std::endl;
  std::cout << "\n";
  try
  {
    Rectangle rect1{10, 10 ,{0, 0}};
    Circle circle1{ { 5, 5 }, 1 };
    Triangle triangle1{ { 20, 20 },{ 30, 30},{ 40, 20 } };
    Rectangle rect2{ 2, 2, {10, 10} };
    Circle circle2{ { 0, 0 }, 3 };
    Triangle triangle2{ { 4, -4 },{ 5, 0 },{ 6, 0 } };
    CompositeShape composite(&rect1);
    composite.addShape(&circle1);
    composite.addShape(&triangle1);
    Matrix matrix(&composite);
    matrix.addShape(&rect2);
    matrix.addShape(&circle2);
    matrix.addShape(&triangle2);
    matrix.printInformation();
  }
  catch (...)
  {
    std::cout << "Errors";
  }

  return 0;
}
