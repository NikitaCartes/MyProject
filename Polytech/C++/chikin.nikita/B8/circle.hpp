#include "shape.hpp"
#include <iostream>
#ifndef CIRCLE_HPP
#define CIRCLE_HPP

class Circle: public Shape
{
public:
  Circle();
  Circle(int x, int y);
  ~Circle()=default;
  using Shape::isMoreLeft;
  using Shape::isUpper;
  void draw()const override;
};

Circle::Circle():
  Shape()
{}
Circle::Circle(int x, int y):
  Shape(x, y)
{}
void::Circle::draw() const
{
  std::cout<<"CIRCLE "<<"("<<m_x<<"; "<<m_y<<")"<<std::endl;
}

#endif // CIRCLE_HPP
