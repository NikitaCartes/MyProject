#include "punct.hpp"
#include <iostream>

int main(int argc, char *argv[])
{
  if (argc!=2)
  {
    std::cerr<<"Wrong value of argc";
    exit(1);
  }
  switch (atoi(argv[1]))
  {
    case 1:
    {
      punct1();
      break;
    }
    case 2:
    {
      punct2();
      break;
    }
    default:
    {
      std::cerr<<"Incorrect argument";
      exit(1);
      break;
    }
  }
  return 0;
}
