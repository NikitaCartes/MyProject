#include "punct.hpp"
#include "triangle.hpp"
#include "square.hpp"
#include "circle.hpp"
#include "shape.hpp"
#include <memory>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <functional>
#include <iterator>
#include <list>

using namespace std::placeholders;

void punct2()
{
  std::string line="";
  std::list<std::shared_ptr<Shape>> list;
  while (true)
  {
    if (std::getline(std::cin, line, '\n'))
    {
      line.erase((std::remove_if(line.begin(), line.end(),[](char c){if (std::isblank(c))return true; return false;})), line.end());
      if (line.empty())
      {
        continue;
      }
      size_t counter=0;
      std::for_each (line.begin(), line.end(), [&counter](char c){ if ((c=='(')||(c==';')||(c==')'))++counter;});
      if (counter!=3)
      {
        std::cerr<<"Missing punctuation";
        exit(1);
      }
      std::replace_if(line.begin(), line.end(), [](char c){ if ((c=='(')||(c==')')||(c==';')){return true;} return false;},' ');
      std::stringstream ss(line);
      std::string str="";
      ss>>str;
      if (!((str=="CIRCLE")||(str=="TRIANGLE")||(str=="SQUARE")))
      {
        std::cerr<<"Wrong figure";
        exit(1);
      }
      std::shared_ptr<Shape>figure;
      if (str== "CIRCLE")
      {
        figure=std::make_shared<Circle>();
      }
      else if (str=="TRIANGLE")
      {
        figure=std::make_shared<Triangle>();
      }
      else
      {
        figure=std::make_shared<Square>();
      }
      counter=0;
      while (ss>>str)
      {
        if (counter>1)
        {
          std::cerr<<"Too many coords";
          exit(1);
        }
        std::string::iterator it=str.begin();
        int size = str.size();
        if (*it=='-')
        {
          ++it;
          --size;
        }
        if ((std::count_if(it, str.end(), [](char c){return (std::isdigit(c));}))==size)
        {
          if (it==str.begin())
          {
            std::string number="";
            number=std::accumulate(it, str.end(), number);
            if (counter==0)
            {
              figure->m_x=stoi(number);
              ++counter;
            }
            else
            {
              figure->m_y=stoi(number);
              ++counter;
            }
          }
          else
          {
            std::string number="";
            --it;
            number=std::accumulate(it, str.end(), number);
            if (counter==0)
            {
              figure->m_x=stoi(number);
              ++counter;
            }
            else
            {
              figure->m_y=stoi(number);
              ++counter;
            }
          }
        }
        else
        {
          std::cerr<<"You enter not a number";
          exit(1);
        }
      }
      if (counter<2)
      {
        std::cerr<<"Not enought coords";
        exit(1);
      }
      list.push_back(figure);
    }
    else
    {
      if (std::cin.eof())
      {
        break;
      }
      std::cerr<<"Incorrect input";
      exit (1);
    }
  }
  if (list.empty())
  {
    exit(0);
  }
  std::function<void(std::shared_ptr<Shape>&)> print=[](std::shared_ptr<Shape>&fig){ fig->draw(); };
  std::cout<<"Original:"<<std::endl;
  std::for_each(list.begin(), list.end(), std::bind(print, _1));
  std::function<bool(std::shared_ptr<Shape>&,std::shared_ptr<Shape>&)>
  sortList=[](std::shared_ptr<Shape>&fig1,std::shared_ptr<Shape>&fig2)->bool
  {
    if (fig1->isMoreLeft(fig2))
    {
      return true;
    }
    return false;
  };
  list.sort(std::bind(sortList, _1, _2));
  std::cout<<"Left-Right:"<<std::endl;
  std::for_each(list.begin(), list.end(), std::bind(print, _1));
  list.sort(std::not2(sortList));
  std::cout<<"Right-Left:"<<std::endl;
  std::for_each(list.begin(), list.end(), std::bind(print, _1));
  std::function<bool(std::shared_ptr<Shape>&,std::shared_ptr<Shape>&)>
  sortListUp=[](std::shared_ptr<Shape>&fig1,std::shared_ptr<Shape>&fig2)->bool
  {
    if (fig1->isUpper(fig2))
    {
      return true;
    }
    return false;
  };
  list.sort(std::bind(sortListUp, _1, _2));
  std::cout<<"Top-Bottom:"<<std::endl;
  std::for_each(list.begin(), list.end(), std::bind(print, _1));
  list.sort(std::not2(sortListUp));
  std::cout<<"Bottom-Top:"<<std::endl;
  std::for_each(list.begin(), list.end(), std::bind(print, _1));
}
