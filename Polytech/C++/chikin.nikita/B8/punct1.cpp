#include "punct.hpp"
#include <algorithm>
#include <iostream>
#include <list>
#include <cmath>
#include <iterator>

#define _USE_MATH_DEFINES

struct  Multiplication
{
  void operator()(long double  &value)
  {
    value=value*M_PI;
  }
};

void punct1()
{
  std::list<long double> list;
  long double value;
  if (!std::cin)
  {
    std::cerr<<"Incorrect input";
    exit(1);
  }
  else
  {
    while (true)
    {
      if (std::cin>>value)
      {
        list.push_back(value);
      }
      else
      {
        if (std::cin.eof())
        {
          break;
        }
        std::cerr<<"Incorrect input";
        exit (1);
      }
    }
  }
  if (list.empty())
  {
    exit(0);
  }
  Multiplication mult;
  std::for_each(list.begin(), list.end(), mult);
  std::copy(list.begin(), list.end(), std::ostream_iterator<long double>(std::cout, " "));
  std::cout << std::endl;
}
