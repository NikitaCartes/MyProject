#include <memory>
#ifndef SHAPE_HPP
#define SHAPE_HPP

class Shape
{
public:
  Shape();
  Shape (int x, int y);
  virtual ~Shape()=default;
  bool isMoreLeft (const std::shared_ptr<Shape>& figure) const;
  bool isUpper  (const std::shared_ptr<Shape>& figure) const;
  virtual void draw () const = 0;
  int m_x;
  int m_y;
};

#endif // SHAPE_HPP
