#include "shape.hpp"
#include <memory>

bool Shape::isMoreLeft(const std::shared_ptr<Shape>& figure) const
{
  if (m_x<figure->m_x)
  {
    return true;
  }
  return false;
}

bool Shape::isUpper(const std::shared_ptr<Shape>& figure) const
{
  if (m_y>figure->m_y)
  {
    return true;
  }
  return false;
}
Shape::Shape():
  m_x(0),
  m_y(0)
{}
Shape::Shape(int x, int y):
  m_x(x),
  m_y(y)
{}
