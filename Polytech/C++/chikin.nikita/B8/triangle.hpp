#include "shape.hpp"
#include <iostream>

#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

class Triangle: public Shape
{
public:
  Triangle();
  Triangle(int x, int y);
  ~Triangle()=default;
  using Shape::isMoreLeft;
  using Shape::isUpper;
  void draw()const override;
};
Triangle::Triangle():
  Shape()
{}
Triangle::Triangle(int x, int y):
  Shape(x,y)
{}
void::Triangle::draw() const
{
  std::cout<<"TRIANGLE "<<"("<<m_x<<"; "<<m_y<<")"<<std::endl;
}

#endif // TRIANGLE_HPP
