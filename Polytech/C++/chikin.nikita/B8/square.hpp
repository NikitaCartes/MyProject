#include "shape.hpp"
#include <iostream>

#ifndef SQUARE_HPP
#define SQUARE_HPP

class Square: public Shape
{
public:
  Square();
  Square(int x, int y);
  ~Square()=default;
  using Shape::isMoreLeft;
  using Shape::isUpper;
  void draw()const override;
};

Square::Square():
  Shape()
{}
Square::Square(int x, int y):
  Shape(x,y)
{}
void::Square::draw() const
{
  std::cout<<"SQUARE "<<"("<<m_x<<"; "<<m_y<<")"<<std::endl;
}
#endif // SQUARE_HPP
