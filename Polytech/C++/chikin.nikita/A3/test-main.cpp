#ifndef BOOST_TEST_MAIN
#define BOOST_TEST_MAIN

#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include "CompositeShape.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>

BOOST_AUTO_TEST_SUITE(CompositeShape_tests)

  BOOST_AUTO_TEST_CASE(CompositeShape_new_null)
  {
    BOOST_CHECK_THROW(CompositeShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_add_null)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    BOOST_CHECK_THROW(composite.addShape(nullptr), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(CompositeShape_area_test)
  {
    double area = 0;
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    area += rectangle.getArea();
    area += triangle.getArea();
    area += circle.getArea();
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_move_cord_test)
  {
    Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    double area = composite.getArea();
    composite.move(15, -10);
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_set_cord_test)
  {
    Rectangle rectangle{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rectangle);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    double area = composite.getArea();
    composite.move({ 15, 10 });
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_centre_move_cord_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    point_t before_pos = composite.getCentre();
    composite.move(15, -10);
    point_t after_pos = composite.getCentre();
    BOOST_REQUIRE_EQUAL(after_pos.x, before_pos.x + 15);
    BOOST_REQUIRE_EQUAL(after_pos.y, before_pos.y - 10);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_centre_set_cord_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.move({ 15, -10 });
    point_t after_pos = composite.getCentre();
    BOOST_REQUIRE_EQUAL(after_pos.x, 15);
    BOOST_REQUIRE_EQUAL(after_pos.y, -10);
  }
  BOOST_AUTO_TEST_CASE(CompositeShape_length_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    BOOST_REQUIRE_EQUAL(composite.getLength(), 1);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    BOOST_REQUIRE_EQUAL(composite.getLength(), 2);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_REQUIRE_EQUAL(composite.getLength(), 3);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_scale_up_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const double area = composite.getArea();
    composite.scale(4);
    BOOST_REQUIRE_EQUAL(composite.getArea(), area * 16);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_scale_down_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    const double area = composite.getArea();
    composite.scale(0.5);
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area /4, 0.1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_scale_zero_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_CHECK_THROW(composite.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_invalid_scale_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_CHECK_THROW(composite.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_invalid_brackets_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_CHECK_THROW(composite[4], std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_valid_brackets_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    rectangle_t originalRect = rect.getFrameRect();
    rectangle_t modifiesRect = composite[0].getFrameRect();
    BOOST_REQUIRE_EQUAL(originalRect.height, modifiesRect.height);
    BOOST_REQUIRE_EQUAL(originalRect.width, modifiesRect.width);
    BOOST_REQUIRE_EQUAL(originalRect.pos.x, modifiesRect.pos.x);
    BOOST_REQUIRE_EQUAL(originalRect.pos.y, modifiesRect.pos.y);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_invalid_number_deleting_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    BOOST_CHECK_THROW(composite.deleteShape(4), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_length_after_deleting_test)
  {
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.deleteShape(1);
    BOOST_REQUIRE_EQUAL(composite.getLength(), 2);
  }

  BOOST_AUTO_TEST_CASE(CompositeShape_area_after_deleting_test)
  {
    double area = 0;
    Rectangle rect{  { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
    CompositeShape composite(&rect);
    Circle circle{ { 5, 5 }, 10 };
    composite.addShape(&circle);
    Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
    composite.addShape(&triangle);
    composite.deleteShape(1);
    area += rect.getArea();
    area += triangle.getArea();
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area, 0.1);
  }
BOOST_AUTO_TEST_SUITE_END()
#endif
