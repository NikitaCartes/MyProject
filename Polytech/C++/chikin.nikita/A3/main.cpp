#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include "CompositeShape.hpp"
#include <string>
#include <iostream>

void printInformation(rectangle_t &frameRect)
{
  std::cout << "x = " << frameRect.pos.x << "\ny = " << frameRect.pos.y <<
    "\nwidth = " << frameRect.width << "\nheight = " << frameRect.height << "\n";
}

int main()
{
  Rectangle rectangle{ { 5, 2.5 },{ 5, 17.5 },{ 15, 17.5 },{ 15, 2.5 } };
  CompositeShape composite(&rectangle);
  Circle circle{ { 5, 5 }, 10};
  composite.addShape(&circle);
  Triangle triangle{ { 0,1 },{ 10,10 },{ 20, 1 } };
  composite.addShape(&triangle);
  composite.printInformation();
  std::cout <<"Area = " << composite.getArea() << "\n\n";
  composite.move(+2.5, -1.25);
  composite.printInformation();
  std::cout << "Area = " << composite.getArea() << "\n\n";
  composite.move({ 0, 0 });
  composite.printInformation();
  std::cout << "Area = " << composite.getArea() << "\n\n";
  composite.scale(2);
  std::cout << "scale, k=2\n\n";
  composite.printInformation();
  std::cout << "Area = " << composite.getArea() << "\n\n";
  composite.deleteShape(1);
  composite.printInformation();
  return 0;
}
