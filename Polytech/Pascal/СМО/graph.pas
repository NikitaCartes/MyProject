Unit graph;

interface
	procedure drawGraph(k: integer);
	procedure finalGraph;
	procedure modelingGraph;
	
implementation
	uses VESA256, Crt, strings, func, inter;
	
	procedure drawGraph;
	var 
		j: integer;
		X, Y: integer;
		Onehundredth: integer;
		{graphArr : array [1..110] of integer;}
	begin
		ClearGraphics(black);
		SetLineMode(7, 3, 1, 0);
		Line(15, 10, 1000, 10);
		Line(15, 160, 1000, 160);
		Line(15, 170, 1000, 170);
		Line(15, 320, 1000, 320);
		Line(15, 330, 1000, 330);
		Line(15, 480, 1000, 480);
		Line(15, 490, 1000, 490);
		Line(15, 640, 1000, 640);
		SetLineMode(8, 3, 1, 0);
		Line(15, 85, 1000, 85);
		Line(15, 245, 1000, 245);
		Line(15, 405, 1000, 405);
		Line(15, 565, 1000, 565);
		MoveTo(15, 160);
		SetLineMode(1, 0, 1, 0);
		Onehundredth:=ThreadTypeArr[k, 1].Onehundredth;
		for j:=1 to 100 do
		begin
			LineTo(15 + j*10, 160 - round(150*(ThreadTypeArr[k, 1].graph[j]/Onehundredth)));
		end;
		
		MoveTo(15, 320);
		SetLineMode(2, 0, 1, 0);
		Onehundredth:=ThreadTypeArr[k, 2].Onehundredth;
		for j:=1 to 100 do
		begin
			LineTo(15 + j*10, 320 - round(150*(ThreadTypeArr[k, 2].graph[j]/Onehundredth)));
		end;
		
		MoveTo(15, 480);
		SetLineMode(3, 0, 1, 0);
		Onehundredth:=ThreadTypeArr[k, 3].Onehundredth;
		for j:=1 to 100 do
		begin
			LineTo(15 + j*10, 480 - round(150*(ThreadTypeArr[k, 3].graph[j]/Onehundredth)));
		end;
		
		MoveTo(15, 640);
		SetLineMode(4, 0, 1, 0);
		Onehundredth:=HandlerArr[k].Onehundredth;
		for j:=1 to 100 do
		begin
			LineTo(15 + j*10, 640 - round(150*(HandlerArr[k].graph[j]/Onehundredth)));
		end;
		WaitForUser;
		Table;
	end;
	
	procedure modelingGraph;
	var 
		k: integer;
		T1, T2, T3, T4: integer;
		i: LongInt;
		j, intkey: integer;
		lambda: real;
	begin
			minRequest:=2000;
			j:=0;
			k:=1;
			ClearGraphics(black);
			WaitForUser;
			SetLineMode(7, 3, 1, 0);
			Line(15, 420, 1000, 420);
			PutText(980,140,StrPCopy('','0.8'), TRUE,1,15,black,0);
			PutText(980,280,StrPCopy('','0.6'), TRUE,1,15,black,0);
			PutText(980,420,StrPCopy('','0.4'), TRUE,1,15,black,0);
			PutText(980,560,StrPCopy('','0.2'), TRUE,1,15,black,0);
			PutText(980,700,StrPCopy('','0'), TRUE,1,15,black,0);
			Line(15, 280, 1000, 280);
			Line(15, 140, 1000, 140);
			Line(15, 560, 1000, 560);
			Line(15, 700, 1000, 700);
			SetLineMode(8, 3, 1, 0);
			Line(1000, 0, 1000, 700);
			Line(250, 0, 250, 700);
			Line(500, 0, 500, 700);
			PutText(240,700,StrPCopy('','500'), TRUE,1,15,black,0);
			PutText(490,700,StrPCopy('','1000'), TRUE,1,15,black,0);
			PutText(740,700,StrPCopy('','1500'), TRUE,1,15,black,0);
			PutText(990,700,StrPCopy('','2000'), TRUE,1,15,black,0);
			Line(750, 0, 750, 700);
			PutText(900,550,StrPCopy('','P_err #1'), TRUE,1,1,black,0);
			PutText(900,590,StrPCopy('','P_err #2'), TRUE,1,2,black,0);
			PutText(900,630,StrPCopy('','P_err #3'), TRUE,1,3,black,0);
			PutText(900,670,StrPCopy('','   K'), TRUE,1,4,black,0);	
			T1:=700;
			T2:=700;
			T3:=700;
			T4:=700;
			randomize;
			lambda:=lambdas[-1]+(j*(lambdas[0] - lambdas[-1]))/10;
			HandlerArr[j].init(lambda);
			ThreadTypeArr[j, 1].init(lambdas[1]);
			ThreadTypeArr[j, 2].init(lambdas[2]);
			ThreadTypeArr[j, 3].init(lambdas[3]);
			i:=1;
			while ((ThreadTypeArr[j, 1].totalRequest <= minRequest) or
					(ThreadTypeArr[j, 2].totalRequest <= minRequest) or
						(ThreadTypeArr[j, 3].totalRequest <= minRequest)) do
			begin
				if ((ThreadTypeArr[j, 1].totalRequest + 
					ThreadTypeArr[j, 2].totalRequest + 
						ThreadTypeArr[j, 3].totalRequest) >= HandlerArr[j].Onehundredth*k) then
				begin
					SetLineMode(4,0,1,0);
					Line(15+10*(k-1), T4, 15+10*k, 700-round(700*(HandlerArr[j].uptime/HandlerArr[j].totaltime)));
					T4:=700-round(700*(HandlerArr[j].uptime/HandlerArr[j].totaltime));
					SetLineMode(1,0,1,0);
					Line(15+10*(k-1), T1, 15+10*k, 700-round(700*((ThreadTypeArr[j, 1].failedRequest)/(ThreadTypeArr[j, 1].totalRequest))));
					T1:=700-round(700*((ThreadTypeArr[j, 1].failedRequest)/(ThreadTypeArr[j, 1].totalRequest)));
					SetLineMode(2,0,1,0);
					Line(15+10*(k-1), T2, 15+10*k, 700-round(700*((ThreadTypeArr[j, 2].failedRequest)/(ThreadTypeArr[j, 2].totalRequest))));
					T2:=700-round(700*((ThreadTypeArr[j, 2].failedRequest)/(ThreadTypeArr[j, 2].totalRequest)));
					SetLineMode(3,0,1,0);
					Line(15+10*(k-1), T3, 15+10*k, 700-round(700*((ThreadTypeArr[j, 3].failedRequest)/(ThreadTypeArr[j, 3].totalRequest))));
					T3:=700-round(700*((ThreadTypeArr[j, 3].failedRequest)/(ThreadTypeArr[j, 3].totalRequest)));
					
					k:=k+1;
				end;
				if HandlerArr[j].endTime <= i then
				begin
					HandlerArr[j].free := true;
				end;
				ThreadTypeArr[j, 1].timeForNew(1, j, i, HandlerArr[j]);
				ThreadTypeArr[j, 2].timeForNew(2, j, i, HandlerArr[j]);
				ThreadTypeArr[j, 3].timeForNew(3, j, i, HandlerArr[j]);
				if HandlerArr[j].free = true then
				begin
					HandlerArr[j].downtime:= HandlerArr[j].downtime + 1;
				end
				else
				begin
					HandlerArr[j].uptime:= HandlerArr[j].uptime + 1;
				end;
				HandlerArr[j].totaltime:= HandlerArr[j].totaltime + 1;
				i:=i+1;
			end;
			WaitForUser;
			drawfunc;
	end;
	
	procedure finalGraph;
	var 
		j: integer;
		X, Y: integer;
		S:string;
	begin
		ClearGraphics(black);
		SetLineMode(7, 3, 1, 0);
		PutText(900,600,StrPCopy('','P_err #1'), TRUE,1,1,black,0);
		PutText(900,640,StrPCopy('','P_err #2'), TRUE,1,2,black,0);
		PutText(900,680,StrPCopy('','P_err #3'), TRUE,1,3,black,0);
		PutText(900,720,StrPCopy('','   K'), TRUE,1,4,black,0);	
		Line(15, 280, 1000, 280);
		Line(15, 140, 1000, 140);
		Line(15, 420, 1000, 420);
		Line(15, 560, 1000, 560);
		Line(15, 700, 1000, 700);
		SetLineMode(8, 3, 1, 0);
		PutText(980,140,StrPCopy('','0.8'), TRUE,1,15,black,0);
		PutText(980,280,StrPCopy('','0.6'), TRUE,1,15,black,0);
		PutText(980,420,StrPCopy('','0.4'), TRUE,1,15,black,0);
		PutText(980,560,StrPCopy('','0.2'), TRUE,1,15,black,0);
		PutText(980,700,StrPCopy('','0'), TRUE,1,15,black,0);
		Line(815, 0, 815, 700);
		Line(735, 0, 735, 700);
		Line(655, 0, 655, 700);
		Line(575, 0, 575, 700);
		Line(495, 0, 495, 700);
		Line(415, 0, 415, 700);
		Line(335, 0, 335, 700);
		Line(255, 0, 255, 700);
		Line(175, 0, 175, 700);
		Line(95, 0, 95, 700);
		Line(15, 0, 15, 700);		
		SetLineMode(1, 0, 1, 0);
		MoveTo(15, 700 - round(700*((ThreadTypeArr[1, 1].failedRequest)/(ThreadTypeArr[1, 1].totalRequest))));
		for j:=1 to 10 do
		begin
			str(HandlerArr[j].lambda:2:1, S);
			PutText(5 + j*80,700,StrPCopy('', S), TRUE,1,15,Black,0);
			LineTo(15 + j*80, 700 - round(700*((ThreadTypeArr[j, 1].failedRequest)/(ThreadTypeArr[j, 1].totalRequest))));
		end;
		SetLineMode(2, 0, 1, 0);
		MoveTo(15, 700 - round(700*((ThreadTypeArr[1, 2].failedRequest)/(ThreadTypeArr[1, 2].totalRequest))));
		for j:=1 to 10 do
		begin
			LineTo(15 + j*80, 700 - round(700*((ThreadTypeArr[j, 2].failedRequest)/(ThreadTypeArr[j, 2].totalRequest))));
		end;
		SetLineMode(3, 0, 1, 0);
		MoveTo(15, 700 - round(700*((ThreadTypeArr[1, 3].failedRequest)/(ThreadTypeArr[1, 3].totalRequest))));
		for j:=1 to 10 do
		begin
			LineTo(15 + j*80, 700 - round(700*((ThreadTypeArr[j, 3].failedRequest)/(ThreadTypeArr[j, 3].totalRequest))));
		end;
		SetLineMode(4, 0, 1, 0);
		MoveTo(15, 700 - round(700*((HandlerArr[0].uptime)/(HandlerArr[0].totaltime))));
		for j:=1 to 10 do
		begin
			LineTo(15 + j*80, 700 - round(700*((HandlerArr[j].uptime)/(HandlerArr[j].totaltime))));
		end;
		WaitForUser;
		Table;
	end;
	
begin
end.