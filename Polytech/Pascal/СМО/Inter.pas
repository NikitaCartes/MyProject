Unit inter;

interface
	Type window = object
		number: integer;
		X, Y, W, H: integer;
		Shown : boolean;
		text : PCHAR;
		procedure draw;
		procedure init(Tnumber: integer; TX, TY, TW, TH: integer; bool : boolean; line : PCHAR);
		procedure click(TX, TY : integer);
	end;
	type windowarr = array [1..14] of window;
	var windows : windowarr;
	var lambdas : array [-1..3] of real;
	var minRequest : integer;
	procedure drawfunc;
	procedure Click(TX, TY : integer);
	procedure DetailedTable;
	procedure Table;
	Function TextArea(line: string; X, Y : integer) : real;
	
implementation
	uses VESA256, func, strings, Crt, graph;
	
	Function TextArea;
	var 
		i: integer;
		r: real;
	begin
		for i:=1 to length(line) do
		begin
			SetLineMode(4,0,1,0);
			Box(X + 200, Y, X + 400, Y + 100);
			PutText(X + 220, Y + 40, StrPCopy('',line), TRUE, 4, 15, Black, 0);
			if line[i]='_' then
			begin
				line[i]:=ReadKey;
			end;
		end;
		i:= 0;
		Val(line, r, i);
		if i <> 0 then
		begin
			r:= TextArea('____', X, Y)
		end;
		TextArea:=r;
	end;
	
	
	procedure Table;
	var 
		S: string;
		j: integer;
	begin
		ClearGraphics(black);	
		PutText(30,20,StrPCopy('','         number'), TRUE,1,15,Black,0);
		PutText(30,60,StrPCopy('','         lambda'), TRUE,1,15,Black,0);
		PutText(30,100,StrPCopy('','    P_err total'), TRUE,1,15,Black,0);
		PutText(30,140,StrPCopy('','       P_err #1'), TRUE,1,15,Black,0);
		PutText(30,180,StrPCopy('','       P_err #2'), TRUE,1,15,Black,0);
		PutText(30,220,StrPCopy('','       P_err #3'), TRUE,1,15,Black,0);
		PutText(30,260,StrPCopy('','              K'), TRUE,1,15,Black,0);		
		PutText(205,20,StrPCopy('','1'), TRUE,1,15,Black,0);
		PutText(280,20,StrPCopy('','2'), TRUE,1,15,Black,0);
		PutText(355,20,StrPCopy('','3'), TRUE,1,15,Black,0);
		PutText(430,20,StrPCopy('','4'), TRUE,1,15,Black,0);
		PutText(505,20,StrPCopy('','5'), TRUE,1,15,Black,0);
		PutText(580,20,StrPCopy('','6'), TRUE,1,15,Black,0);
		PutText(655,20,StrPCopy('','7'), TRUE,1,15,Black,0);
		PutText(730,20,StrPCopy('','8'), TRUE,1,15,Black,0);
		PutText(805,20,StrPCopy('','9'), TRUE,1,15,Black,0);
		PutText(875,20,StrPCopy(' ','10'), TRUE,1,15,Black,0);
		PutText(950,20,StrPCopy(' ','11'), TRUE,1,15,Black,0);
		SetLineMode(15,0,1,0);
		Line(45, 18, 45, 285);
		Line(170, 18, 170, 285);
		Line(243, 18, 243, 285);
		Line(317, 18, 317, 285);
		Line(392, 18, 392, 285);
		Line(467, 18, 467, 285);
		Line(542, 18, 542, 285);
		Line(617, 18, 617, 285);
		Line(692, 18, 692, 285);
		Line(767, 18, 767, 285);
		Line(842, 18, 842, 285);
		Line(917, 18, 917, 285);
		Line(992, 18, 992, 285);
		Line(45, 18, 992, 18);
		Line(45, 45, 992, 45);
		Line(45, 245, 992, 245);
		Line(45, 85, 992, 85);
		Line(45, 125, 992, 125);
		Line(45, 165, 992, 165);
		Line(45, 205, 992, 205);
		Line(45, 245, 992, 245);
		Line(45, 285, 992, 285);
		for j:=0 to 10 do
		begin
			str(HandlerArr[j].lambda:2:1, S);
			PutText(115+75*(j+1),60,StrPCopy('', S), TRUE,1,15,Black,0);
			str(((100.0 * HandlerArr[j].failedRequest)/(HandlerArr[j].totalRequest)):2:2, S);
			PutText(110+75*(j+1),100,StrPCopy('', S + '%'), TRUE,1,15,Black,0);
			str(((100.0 * ThreadTypeArr[j, 1].failedRequest)/(ThreadTypeArr[j, 1].totalRequest)):2:2, S);
			PutText(110+75*(j+1),140,StrPCopy('', S + '%'), TRUE,1,15,Black,0);
			str(((100.0 * ThreadTypeArr[j, 2].failedRequest)/(ThreadTypeArr[j, 2].totalRequest)):2:2, S);
			PutText(110+75*(j+1),180,StrPCopy('', S + '%'), TRUE,1,15,Black,0);
			str(((100.0 * ThreadTypeArr[j, 3].failedRequest)/(ThreadTypeArr[j, 3].totalRequest)):2:2, S);
			PutText(105+75*(j+1),220,StrPCopy('', S + '%'), TRUE,1,15,Black,0);
			str(((100.0 * HandlerArr[j].uptime)/(HandlerArr[j].totaltime)):2:2, S);
			PutText(110+75*(j+1),260,StrPCopy('', S + '%'), TRUE,1,15,Black,0);
		end;
		WaitForUserGuiTable;
	end;
	
	procedure DetailedTable;
	var 
		S: string;
		j: integer;
	begin
		ClearGraphics(black);
		PutText(30,95,StrPCopy('','H'), TRUE,1,4,Black,0);
		PutText(30,110,StrPCopy('','A'), TRUE,1,4,Black,0);
		PutText(30,125,StrPCopy('','N'), TRUE,1,4,Black,0);
		PutText(30,140,StrPCopy('','D'), TRUE,1,4,Black,0);
		PutText(30,155,StrPCopy('','L'), TRUE,1,4,Black,0);
		PutText(30,170,StrPCopy('','E'), TRUE,1,4,Black,0);
		PutText(30,185,StrPCopy('','R'), TRUE,1,4,Black,0);
		PutText(30,260,StrPCopy('','T'), TRUE,1,2,Black,0);
		PutText(30,273,StrPCopy('','H'), TRUE,1,2,Black,0);
		PutText(30,286,StrPCopy('','R'), TRUE,1,2,Black,0);
		PutText(30,299,StrPCopy('','E'), TRUE,1,2,Black,0);
		PutText(30,312,StrPCopy('','A'), TRUE,1,2,Black,0);
		PutText(30,325,StrPCopy('','D'), TRUE,1,2,Black,0);
		PutText(30,338,StrPCopy('','1'), TRUE,1,2,Black,0);
		PutText(30,380,StrPCopy('','T'), TRUE,1,3,Black,0);
		PutText(30,393,StrPCopy('','H'), TRUE,1,3,Black,0);
		PutText(30,406,StrPCopy('','R'), TRUE,1,3,Black,0);
		PutText(30,419,StrPCopy('','E'), TRUE,1,3,Black,0);
		PutText(30,432,StrPCopy('','A'), TRUE,1,3,Black,0);
		PutText(30,445,StrPCopy('','D'), TRUE,1,3,Black,0);
		PutText(30,458,StrPCopy('','2'), TRUE,1,3,Black,0);
		PutText(30,500,StrPCopy('','T'), TRUE,1,1,Black,0);
		PutText(30,513,StrPCopy('','H'), TRUE,1,1,Black,0);
		PutText(30,526,StrPCopy('','R'), TRUE,1,1,Black,0);
		PutText(30,539,StrPCopy('','E'), TRUE,1,1,Black,0);
		PutText(30,552,StrPCopy('','A'), TRUE,1,1,Black,0);
		PutText(30,565,StrPCopy('','D'), TRUE,1,1,Black,0);
		PutText(30,578,StrPCopy('','3'), TRUE,1,1,Black,0);		
		PutText(50,20,StrPCopy('','         number'), TRUE,1,15,Black,0);
		PutText(50,60,StrPCopy('','         lambda'), TRUE,1,15,Black,0);
		PutText(50,100,StrPCopy('','  total request'), TRUE,1,15,Black,0);
		PutText(50,140,StrPCopy('','     successful'), TRUE,1,15,Black,0);
		PutText(50,180,StrPCopy('','         failed'), TRUE,1,15,Black,0);
		PutText(50,220,StrPCopy('','          P_err'), TRUE,1,14,Black,0);
		PutText(50,260,StrPCopy('','  total request'), TRUE,1,15,Black,0);
		PutText(50,300,StrPCopy('','     successful'), TRUE,1,15,Black,0);
		PutText(50,340,StrPCopy('','         failed'), TRUE,1,15,Black,0);
		PutText(50,380,StrPCopy('','  total request'), TRUE,1,15,Black,0);
		PutText(50,420,StrPCopy('','     successful'), TRUE,1,15,Black,0);
		PutText(50,460,StrPCopy('','         failed'), TRUE,1,15,Black,0);
		PutText(50,500,StrPCopy('','  total request'), TRUE,1,15,Black,0);
		PutText(50,540,StrPCopy('','     successful'), TRUE,1,15,Black,0);
		PutText(50,580,StrPCopy('','         failed'), TRUE,1,15,Black,0);
		PutText(50,620,StrPCopy('','     total time'), TRUE,1,15,Black,0);
		PutText(50,660,StrPCopy('','     downtime %'), TRUE,1,15,Black,0);
		PutText(50,700,StrPCopy('','              K'), TRUE,1,14,Black,0);		
		PutText(205,20,StrPCopy('','1'), TRUE,1,15,Black,0);
		PutText(280,20,StrPCopy('','2'), TRUE,1,15,Black,0);
		PutText(355,20,StrPCopy('','3'), TRUE,1,15,Black,0);
		PutText(430,20,StrPCopy('','4'), TRUE,1,15,Black,0);
		PutText(505,20,StrPCopy('','5'), TRUE,1,15,Black,0);
		PutText(580,20,StrPCopy('','6'), TRUE,1,15,Black,0);
		PutText(655,20,StrPCopy('','7'), TRUE,1,15,Black,0);
		PutText(730,20,StrPCopy('','8'), TRUE,1,15,Black,0);
		PutText(805,20,StrPCopy('','9'), TRUE,1,15,Black,0);
		PutText(875,20,StrPCopy(' ','10'), TRUE,1,15,Black,0);
		PutText(950,20,StrPCopy(' ','11'), TRUE,1,15,Black,0);
		SetLineMode(15,0,1,0);
		Line(18, 18, 18, 720);
		Line(45, 18, 45, 720);
		Line(170, 18, 170, 720);
		Line(243, 18, 243, 720);
		Line(317, 18, 317, 720);
		Line(392, 18, 392, 720);
		Line(467, 18, 467, 720);
		Line(542, 18, 542, 720);
		Line(617, 18, 617, 720);
		Line(692, 18, 692, 720);
		Line(767, 18, 767, 720);
		Line(842, 18, 842, 720);
		Line(917, 18, 917, 720);
		Line(992, 18, 992, 720);
		Line(18, 18, 992, 18);
		Line(18, 45, 992, 45);
		Line(18, 245, 992, 245);
		Line(18, 365, 992, 365);
		Line(18, 485, 992, 485);
		Line(18, 605, 992, 605);
		Line(18, 720, 992, 720);
		Line(45, 85, 992, 85);
		Line(45, 125, 992, 125);
		Line(45, 165, 992, 165);
		Line(45, 205, 992, 205);
		Line(45, 245, 992, 245);
		Line(45, 285, 992, 285);
		Line(45, 325, 992, 325);
		Line(45, 365, 992, 365);
		Line(45, 405, 992, 405);
		Line(45, 445, 992, 445);
		Line(45, 485, 992, 485);
		Line(45, 525, 992, 525);
		Line(45, 565, 992, 565);
		Line(45, 605, 992, 605);
		Line(45, 645, 992, 645);
		Line(45, 685, 992, 685);
		for j:=0 to 10 do
		begin
			str(HandlerArr[j].lambda:2:1, S);
			PutText(115+75*(j+1),60,StrPCopy('', S), TRUE,1,15,Black,0);
			str(HandlerArr[j].totalRequest, S);
			PutText(110+75*(j+1),100,StrPCopy('', S), TRUE,1,15,Black,0);
			str(HandlerArr[j].successfulRequest, S);
			PutText(110+75*(j+1),140,StrPCopy('', S), TRUE,1,15,Black,0);
			str(HandlerArr[j].failedRequest, S);
			PutText(110+75*(j+1),180,StrPCopy('', S), TRUE,1,15,Black,0);
			str(((100.0 * HandlerArr[j].failedRequest)/(HandlerArr[j].totalRequest)):2:2, S);
			PutText(105+75*(j+1),220,StrPCopy('', S + '%'), TRUE,1,14,Black,0);
			str(ThreadTypeArr[j, 1].totalRequest, S);
			PutText(110+75*(j+1),260,StrPCopy('', S), TRUE,1,15,Black,0);
			str(ThreadTypeArr[j, 1].successfulRequest, S);
			PutText(110+75*(j+1),300,StrPCopy('', S), TRUE,1,15,Black,0);
			str(ThreadTypeArr[j, 1].failedRequest, S);
			PutText(110+75*(j+1),340,StrPCopy('', S), TRUE,1,15,Black,0);
			str(ThreadTypeArr[j, 2].totalRequest, S);
			PutText(110+75*(j+1),380,StrPCopy('', S), TRUE,1,15,Black,0);
			str(ThreadTypeArr[j, 2].successfulRequest, S);
			PutText(110+75*(j+1),420,StrPCopy('', S), TRUE,1,15,Black,0);
			str(ThreadTypeArr[j, 2].failedRequest, S);
			PutText(110+75*(j+1),460,StrPCopy('', S), TRUE,1,15,Black,0);
			str(ThreadTypeArr[j, 3].totalRequest, S);
			PutText(110+75*(j+1),500,StrPCopy('', S), TRUE,1,15,Black,0);
			str(ThreadTypeArr[j, 3].successfulRequest, S);
			PutText(110+75*(j+1),540,StrPCopy('', S), TRUE,1,15,Black,0);
			str(ThreadTypeArr[j, 3].failedRequest, S);
			PutText(110+75*(j+1),580,StrPCopy('', S), TRUE,1,15,Black,0);
			str(HandlerArr[j].totaltime, S);
			PutText(105+75*(j+1),620,StrPCopy('', S), TRUE,1,15,Black,0);
			str(((100.0 * HandlerArr[j].downtime)/(HandlerArr[j].totaltime)):2:2, S);
			PutText(105+75*(j+1),660,StrPCopy('', S + '%'), TRUE,1,15,Black,0);
			str(((100.0 * HandlerArr[j].uptime)/(HandlerArr[j].totaltime)):2:2, S);
			PutText(105+75*(j+1),700,StrPCopy('', S + '%'), TRUE,1,14,Black,0);
		end;
		WaitForUserGui;
	end;
	
	procedure window.init; 
	begin
		number:= Tnumber;
		X:= TX;
		Y:= TY;
		H:= TH;
		W:= TW;
		text:= line;
		Shown:=bool;
	end;
	
	procedure window.draw; 
	begin
		if Shown = true then
		begin
			SetLineMode(15,0,3,0);
			Box(X, Y, X + W, Y + H);
			PutText(X+45,Y+10,text,TRUE,1,15,Black,0);
		end;
	end;
	
	procedure window.click;
	begin
		if ((TX >= X) and (TX <= (X + W)) and (TY >= Y) and (TY <= (Y + H)) and (Shown = true)) then
		begin
			case number of
			1:
				begin
					windows[5].Shown:= true;
					windows[6].Shown:= true;
					windows[7].Shown:= true;
					windows[8].Shown:= true;
					windows[9].Shown:= true;
					windows[10].Shown:= true;
					windows[11].Shown:= false;
					windows[12].Shown:= false;
					windows[13].Shown:= false;
					windows[14].Shown:= false;
				end;
			2:
				begin
					windows[5].Shown:= false;
					windows[6].Shown:= false;
					windows[7].Shown:= false;
					windows[8].Shown:= false;
					windows[9].Shown:= false;
					windows[10].Shown:= false;
					windows[11].Shown:= false;
					windows[12].Shown:= false;
					windows[13].Shown:= true;
					windows[14].Shown:= true;
				end;
			3:
				begin
					windows[5].Shown:= false;
					windows[6].Shown:= false;
					windows[7].Shown:= false;
					windows[8].Shown:= false;
					windows[9].Shown:= false;
					windows[10].Shown:= false;
					windows[11].Shown:= true;
					windows[12].Shown:= true;
					windows[13].Shown:= false;
					windows[14].Shown:= false;
				end;
			4:
				begin
					ClearGraphics(black);
					PutText(300,300,'I can tell you about this program',TRUE,1,4,Black,0);
					WaitForUserGui;
				end;
			5:
				begin
					lambdas[-1]:= TextArea('____', X, Y);
				end;
			6:
				begin
					lambdas[0]:= TextArea('____', X, Y);
				end;
			7:
				begin
					lambdas[1]:= TextArea('____', X, Y);
				end;
			8:
				begin
					lambdas[2]:= TextArea('____', X, Y);
				end;
			9:
				begin
					lambdas[3]:= TextArea('____', X, Y);
				end;
			10:
				begin
					minRequest:= trunc(TextArea('____', X, Y));
				end;
			11:
				begin
					Table;
				end;
			12:
				begin
					DetailedTable;
				end;
			13:
				begin
					windows[5].Shown:= false;
					windows[6].Shown:= false;
					windows[7].Shown:= false;
					windows[8].Shown:= false;
					windows[9].Shown:= false;
					windows[10].Shown:= false;
					windows[11].Shown:= false;
					windows[12].Shown:= false;
					windows[13].Shown:= false;
					windows[14].Shown:= false;
					drawfunc;
					Modeling;
				end;
			14:
				begin
					windows[5].Shown:= false;
					windows[6].Shown:= false;
					windows[7].Shown:= false;
					windows[8].Shown:= false;
					windows[9].Shown:= false;
					windows[10].Shown:= false;
					windows[11].Shown:= false;
					windows[12].Shown:= false;
					windows[13].Shown:= false;
					windows[14].Shown:= false;
					modelingGraph;
				end;
			end;
		end;
	end;
	
	procedure drawfunc;
	var j: integer;
	begin
		ClearGraphics(black);
		for j:= 1 to 14 do
		begin
			windows[j].draw;
		end;
	end;
	
	procedure Click;
	var j: integer;
	begin
		for j:= 1 to 14 do
		begin
			windows[j].click(TX, TY);
		end;
		if TX > 500 then
		begin
			windows[5].Shown:= false;
			windows[6].Shown:= false;
			windows[7].Shown:= false;
			windows[8].Shown:= false;
			windows[9].Shown:= false;
			windows[10].Shown:= false;
			windows[11].Shown:= false;
			windows[12].Shown:= false;
		end;
		drawfunc;
	end;
	
	
begin
	lambdas[-1]:= 1.0;
	lambdas[0]:= 2.0;
	lambdas[1]:= 2.0;
	lambdas[2]:= 1.0;
	lambdas[3]:= 5.0;
	minRequest:= 500;
	windows[1].init(1, 100, 100, 200, 100, true, 'Settings');
	windows[2].init(2, 100, 200, 200, 100, true, 'Modeling');
	windows[3].init(3, 100, 300, 200, 100, true, 'Tables');
	windows[4].init(4, 100, 400, 200, 100, true, 'Help');
	windows[5].init(5, 300, 0, 200, 100, false, 'lambda_S');
	windows[6].init(6, 300, 100, 200, 100, false, 'lambda_F');
	windows[7].init(7, 300, 200, 200, 100, false, 'lambda1');
	windows[8].init(8, 300, 300, 200, 100, false, 'lambda2');
	windows[9].init(9, 300, 400, 200, 100, false, 'lambda3');
	windows[10].init(10, 300, 500, 200, 100, false, 'KMIN');
	windows[11].init(11, 300, 300, 200, 100, false, 'Final table');
	windows[12].init(12, 300, 400, 200, 100, false, 'Detailed table');
	windows[13].init(13, 300, 200, 200, 100, false, 'Without graph');
	windows[14].init(14, 300, 300, 200, 100, false, 'With graph');
end.