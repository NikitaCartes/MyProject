Unit func;

interface
	
uses inter, Crt, VESA256, graph;
	
	type
	HandlerType = object
		lambda : Real;
		source : integer;
		free : boolean;
		startTime : LongInt;
		endTime : LongInt;
		totalRequest : integer;
		successfulRequest : integer;
		failedRequest : integer;
		downtime : LongInt;
		uptime : LongInt;
		totaltime : LongInt;
		k : integer;
		k1 : integer;
		Onehundredth: integer;
		graph : array [1..110] of integer;
		procedure init(lambda_: real);
	end;
	
	type
	ThreadType = object
		lambda : Real;
		startTime : LongInt;
		endTime : LongInt;
		totalRequest : integer;
		successfulRequest : integer;
		failedRequest: integer;
		k : integer;
		k1 : integer;
		Onehundredth: integer;
		graph : array [1..110] of integer;
		procedure init(lambda_: Real);
		procedure genRequest(i : LongInt; var Handler : HandlerType);
		procedure timeForNew(t, j: integer; i : LongInt; var Handler : HandlerType);
	end;
	
	var ThreadTypeArr : array [0..10, 1..3] of ThreadType; 
	var HandlerArr : array [0..10] of HandlerType;
	
	procedure WaitForUser;
	procedure WaitForUserGui;
	procedure WaitForUserGuiTable;
	procedure Modeling;
	
implementation
	
	procedure HandlerType.init;
	var i: integer;
	begin
		lambda := lambda_;
		free := true;
		startTime := 0;
		endTime := 0;
		totalRequest := 0;
		successfulRequest := 0;
		failedRequest := 0;
		downtime := 0;
		uptime := 0;
		totaltime := 0;
		k:= 1;
		k1:=0;
		Onehundredth:= round((minRequest*(lambdas[1] + lambdas[2] + lambdas[3]))/100);
		for i:=1 to 100 do
		begin
			graph[i]:=0;
		end;
	end;
	
	procedure ThreadType.init;
	var i: integer;
	begin
		lambda := lambda_;
		startTime := random(1000);
		endTime := startTime + round((-1000*ln(Random*Random))/(lambda));
		successfulRequest:= 0;
		failedRequest:=0;
		totalRequest:=0;
		k:= 1;
		k1:=0;
		Onehundredth:= round((minRequest*(lambda_))/100);
		for i:=1 to 100 do
		begin
			graph[i]:=0;
		end;
	end;
	
	procedure ThreadType.genRequest;
	begin
		totalRequest:=totalRequest + 1;
		Handler.totalRequest:= Handler.totalRequest + 1;
		k1:= k1+1;
		Handler.k1 := Handler.k1+1;
		startTime := i;
		endTime := startTime + round((-1000*ln(Random*Random))/(lambda));
	end;
	
	procedure ThreadType.timeForNew;
	begin
		if endTime < i then
		begin
			genRequest(i, Handler);
			Handler.failedRequest:= Handler.failedRequest + 1;
			Handler.graph[Handler.k]:= Handler.graph[Handler.k] + 1;
			failedRequest:= failedRequest + 1;
			graph[k]:= graph[k] + 1;
			if (Handler.k1 >= Handler.Onehundredth) then
			begin
				Handler.k:=Handler.k+1;
				Handler.k1:=0;
			end;
			if (k1 >= Onehundredth) then
			begin
				k:=k+1;
				k1:=0;
			end;
		end
		else if endTime = i then
		begin
			genRequest(i, Handler);
			if Handler.free = true then
			begin
				Handler.successfulRequest:= Handler.successfulRequest + 1;
				Handler.source:= t;
				Handler.startTime:= i;
				Handler.endTime:= i + round((-1000*ln(Random))/(Handler.lambda));
				Handler.free:= false;
				successfulRequest:= successfulRequest + 1;
			end
			else
			begin
				if Handler.source < t then
				begin
					ThreadTypeArr[j, Handler.source].successfulRequest:= ThreadTypeArr[j, Handler.source].successfulRequest - 1;
					ThreadTypeArr[j, Handler.source].failedRequest:= ThreadTypeArr[j, Handler.source].failedRequest + 1;
					Handler.failedRequest:= Handler.failedRequest + 1;
					Handler.source:= t;
					Handler.startTime:= i;
					Handler.endTime:= i + round((-1000*ln(Random))/(Handler.lambda));
					successfulRequest:= successfulRequest + 1;
				end
				else
				begin
					Handler.failedRequest:= Handler.failedRequest + 1;
					Handler.graph[Handler.k]:= Handler.graph[Handler.k] + 1;
					failedRequest:= failedRequest + 1;
					graph[k]:= graph[k] + 1;
					if (Handler.k1 >= Handler.Onehundredth) then
					begin
						Handler.k:=Handler.k+1;
						Handler.k1:=0;
					end;
					if (k1 >= Onehundredth) then
					begin
						k:=k+1;
						k1:=0;
					end;
				end;
			end;
		end;
	end;
	
	procedure WaitForUser;
	var intkey : integer;
	begin
		ShowMouse;
		repeat
			MoveMouse
		until not IsMouseButtonDown;
		repeat
			MoveMouse;
			if KeyPressed then
			begin
				intkey:= Ord(readkey);
				if intkey = 27 then
				begin
					HideMouse;
					break;
				end
				else
				begin
					HideMouse;
					break;
				end;
			end;
		until IsMouseButtonDown;
		HideMouse;
	end;
	
	procedure WaitForUserGui;
	var 
		intkey : integer;
		X,Y:integer;
	begin
		ShowMouse;
		repeat
			MoveMouse
		until not IsMouseButtonDown;
		repeat
			MoveMouse;
			if KeyPressed then
			begin
				intkey:= Ord(readkey);
				if intkey = 27 then
				begin
					HideMouse;
					break;
				end;
			end;
			if (IsMouseButtonDown) then
			begin
				GetMouseXY(X, Y);
				Click(X, Y);
			end;
		until false;
		HideMouse;
	end;
	
	procedure WaitForUserGuiTable;
	var 
		intkey : integer;
		X,Y:integer;
	begin
		ShowMouse;
		repeat
			MoveMouse
		until not IsMouseButtonDown;
		repeat
			MoveMouse;
			if KeyPressed then
			begin
				intkey:= Ord(readkey);
				if intkey = 27 then
				begin
					break;
				end;
			end;
			if (IsMouseButtonDown) then
			begin
				GetMouseXY(X, Y);
				if (Y > 285) then
				begin
					finalGraph;
				end
				else
				begin
					drawGraph(Trunc((X-92)/75)-1);
				end;
			end;
		until false;
		HideMouse;
	end;
	
	procedure Modeling;
	var 
		i: LongInt;
		j, intkey: integer;
		lambda: real;
	begin
		SetLineMode(4,0,1,0);
		for j:=0 to 10 do
		begin
			Box(400 + j*15, 245, 410 + j*15, 255);
		end;
		SetFillMode(2, 0, 1);
		for j:=0 to 10 do
		begin
			randomize;
			lambda:=lambdas[-1]+(j*(lambdas[0] - lambdas[-1]))/10;
			HandlerArr[j].init(lambda);
			ThreadTypeArr[j, 1].init(lambdas[1]);
			ThreadTypeArr[j, 2].init(lambdas[2]);
			ThreadTypeArr[j, 3].init(lambdas[3]);
			i:=1;
			while ((ThreadTypeArr[j, 1].totalRequest <= minRequest) or
					(ThreadTypeArr[j, 2].totalRequest <= minRequest) or
						(ThreadTypeArr[j, 3].totalRequest <= minRequest)) do
			begin
				if HandlerArr[j].endTime <= i then
				begin
					HandlerArr[j].free := true;
				end;
				ThreadTypeArr[j, 1].timeForNew(1, j, i, HandlerArr[j]);
				ThreadTypeArr[j, 2].timeForNew(2, j, i, HandlerArr[j]);
				ThreadTypeArr[j, 3].timeForNew(3, j, i, HandlerArr[j]);
				if HandlerArr[j].free = true then
				begin
					HandlerArr[j].downtime:= HandlerArr[j].downtime + 1;
				end
				else
				begin
					HandlerArr[j].uptime:= HandlerArr[j].uptime + 1;
				end;
				HandlerArr[j].totaltime:= HandlerArr[j].totaltime + 1;
				i:=i+1;
				if KeyPressed then
				begin
					intkey:= Ord(readkey);
					if intkey = 27 then
					begin
						exit;
					end;
				end;
			end;
			Flood(405 + j*15, 250, 4);
		end;
	end;
	
begin
end.