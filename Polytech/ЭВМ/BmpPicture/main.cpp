#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include "Headers.hpp"

struct Pixel
{
  unsigned char red;
  unsigned char green;
  unsigned char blue;
};

struct PixelError
{
  signed short red;
  signed short green;
  signed short blue;
};

struct PixelMaxMin
{
  unsigned char redMax;
  unsigned char redMin;
  unsigned char greenMax;
  unsigned char greenMin;
  unsigned char blueMax;
  unsigned char blueMin;
};

struct PixelStat
{
  unsigned long int pixel;
  unsigned long int number;
};

struct AfterCut
{
  std::vector < PixelStat > firstHalf;
  std::vector < PixelStat > secondHalf;
  PixelMaxMin firstVector;
  PixelMaxMin secondVector;
};

void Cut(std::vector < PixelStat >const & beforeCut, PixelMaxMin const & pixelMaxMin, AfterCut & afterCut)
{
  /*double red = 0.2126 * (pixelMaxMin.redMax - pixelMaxMin.redMin);
  double green = 0.7152 * (pixelMaxMin.greenMax - pixelMaxMin.greenMin);
  double blue = 0.0722 * (pixelMaxMin.blueMax - pixelMaxMin.blueMin);*/
  double red = (pixelMaxMin.redMax - pixelMaxMin.redMin);
  double green = (pixelMaxMin.greenMax - pixelMaxMin.greenMin);
  double blue = (pixelMaxMin.blueMax - pixelMaxMin.blueMin);
  afterCut.firstVector = { 0,255,0,255,0,255 };
  afterCut.secondVector = { 0,255,0,255,0,255 };
  if ((red >= blue) && (red >= green))
  {
    red = (pixelMaxMin.redMax + pixelMaxMin.redMin) * 32768;
    unsigned long int i1 = 0;
    unsigned long int i2 = 0;
    for (auto k : beforeCut)
    {
      if ((k.pixel & 0x00ff0000) < red)
      {
        afterCut.firstHalf.push_back({ k.pixel, k.number });
        i1++;
        afterCut.firstVector.redMax = (((k.pixel & 0x00ff0000) >> 16) > afterCut.firstVector.redMax) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.firstVector.redMax;
        afterCut.firstVector.redMin = (((k.pixel & 0x00ff0000) >> 16) < afterCut.firstVector.redMin) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.firstVector.redMin;
        afterCut.firstVector.greenMax = (((k.pixel & 0x0000ff00) >> 8) > afterCut.firstVector.greenMax) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.firstVector.greenMax;
        afterCut.firstVector.greenMin = (((k.pixel & 0x0000ff00) >> 8) < afterCut.firstVector.greenMin) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.firstVector.greenMin;
        afterCut.firstVector.blueMax = ((k.pixel & 0x000000ff) > afterCut.firstVector.blueMax) ? (k.pixel & 0x000000ff) : afterCut.firstVector.blueMax;
        afterCut.firstVector.blueMin = ((k.pixel & 0x000000ff) < afterCut.firstVector.blueMin) ? (k.pixel & 0x000000ff) : afterCut.firstVector.blueMin;
      }
      else
      {
        afterCut.secondHalf.push_back({ k.pixel, k.number });
        i2++;
        afterCut.secondVector.redMax = (((k.pixel & 0x00ff0000) >> 16) > afterCut.secondVector.redMax) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.secondVector.redMax;
        afterCut.secondVector.redMin = (((k.pixel & 0x00ff0000) >> 16) < afterCut.secondVector.redMin) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.secondVector.redMin;
        afterCut.secondVector.greenMax = (((k.pixel & 0x0000ff00) >> 8) > afterCut.secondVector.greenMax) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.secondVector.greenMax;
        afterCut.secondVector.greenMin = (((k.pixel & 0x0000ff00) >> 8) < afterCut.secondVector.greenMin) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.secondVector.greenMin;
        afterCut.secondVector.blueMax = ((k.pixel & 0x000000ff) > afterCut.secondVector.blueMax) ? (k.pixel & 0x000000ff) : afterCut.secondVector.blueMax;
        afterCut.secondVector.blueMin = ((k.pixel & 0x000000ff) < afterCut.secondVector.blueMin) ? (k.pixel & 0x000000ff) : afterCut.secondVector.blueMin;
      }
    }
    return;
  }
  if ((blue >= red) && (blue >= green))
  {
    blue = (pixelMaxMin.blueMax + pixelMaxMin.blueMin) / 2;
    unsigned long int i1 = 0;
    unsigned long int i2 = 0;
    for (auto k : beforeCut)
    {
      if ((k.pixel & 0x000000ff) < blue)
      {
        afterCut.firstHalf.push_back({ k.pixel, k.number });
        i1++;
        afterCut.firstVector.redMax = (((k.pixel & 0x00ff0000) >> 16) > afterCut.firstVector.redMax) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.firstVector.redMax;
        afterCut.firstVector.redMin = (((k.pixel & 0x00ff0000) >> 16) < afterCut.firstVector.redMin) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.firstVector.redMin;
        afterCut.firstVector.greenMax = (((k.pixel & 0x0000ff00) >> 8) > afterCut.firstVector.greenMax) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.firstVector.greenMax;
        afterCut.firstVector.greenMin = (((k.pixel & 0x0000ff00) >> 8) < afterCut.firstVector.greenMin) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.firstVector.greenMin;
        afterCut.firstVector.blueMax = ((k.pixel & 0x000000ff) > afterCut.firstVector.blueMax) ? (k.pixel & 0x000000ff) : afterCut.firstVector.blueMax;
        afterCut.firstVector.blueMin = ((k.pixel & 0x000000ff) < afterCut.firstVector.blueMin) ? (k.pixel & 0x000000ff) : afterCut.firstVector.blueMin;
      }
      else
      {
        afterCut.secondHalf.push_back({ k.pixel, k.number });
        i2++;
        afterCut.secondVector.redMax = (((k.pixel & 0x00ff0000) >> 16) > afterCut.secondVector.redMax) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.secondVector.redMax;
        afterCut.secondVector.redMin = (((k.pixel & 0x00ff0000) >> 16) < afterCut.secondVector.redMin) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.secondVector.redMin;
        afterCut.secondVector.greenMax = (((k.pixel & 0x0000ff00) >> 8) > afterCut.secondVector.greenMax) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.secondVector.greenMax;
        afterCut.secondVector.greenMin = (((k.pixel & 0x0000ff00) >> 8) < afterCut.secondVector.greenMin) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.secondVector.greenMin;
        afterCut.secondVector.blueMax = ((k.pixel & 0x000000ff) > afterCut.secondVector.blueMax) ? (k.pixel & 0x000000ff) : afterCut.secondVector.blueMax;
        afterCut.secondVector.blueMin = ((k.pixel & 0x000000ff) < afterCut.secondVector.blueMin) ? (k.pixel & 0x000000ff) : afterCut.secondVector.blueMin;
      }
    }
    return;
  }
  if ((green >= red) && (green >= blue))
  {
    green = (pixelMaxMin.greenMax + pixelMaxMin.greenMin) * 128;
    unsigned long int i1 = 0;
    unsigned long int i2 = 0;
    for (auto k : beforeCut)
    {
      if ((k.pixel & 0x0000ff00) < green)
      {
        afterCut.firstHalf.push_back({ k.pixel, k.number });
        i1++;
        afterCut.firstVector.redMax = (((k.pixel & 0x00ff0000) >> 16) > afterCut.firstVector.redMax) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.firstVector.redMax;
        afterCut.firstVector.redMin = (((k.pixel & 0x00ff0000) >> 16) < afterCut.firstVector.redMin) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.firstVector.redMin;
        afterCut.firstVector.greenMax = (((k.pixel & 0x0000ff00) >> 8) > afterCut.firstVector.greenMax) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.firstVector.greenMax;
        afterCut.firstVector.greenMin = (((k.pixel & 0x0000ff00) >> 8) < afterCut.firstVector.greenMin) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.firstVector.greenMin;
        afterCut.firstVector.blueMax = ((k.pixel & 0x000000ff) > afterCut.firstVector.blueMax) ? (k.pixel & 0x000000ff) : afterCut.firstVector.blueMax;
        afterCut.firstVector.blueMin = ((k.pixel & 0x000000ff) < afterCut.firstVector.blueMin) ? (k.pixel & 0x000000ff) : afterCut.firstVector.blueMin;
      }
      else
      {
        afterCut.secondHalf.push_back({ k.pixel, k.number });
        i2++;
        afterCut.secondVector.redMax = (((k.pixel & 0x00ff0000) >> 16) > afterCut.secondVector.redMax) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.secondVector.redMax;
        afterCut.secondVector.redMin = (((k.pixel & 0x00ff0000) >> 16) < afterCut.secondVector.redMin) ? ((k.pixel & 0x00ff0000) >> 16) : afterCut.secondVector.redMin;
        afterCut.secondVector.greenMax = (((k.pixel & 0x0000ff00) >> 8) > afterCut.secondVector.greenMax) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.secondVector.greenMax;
        afterCut.secondVector.greenMin = (((k.pixel & 0x0000ff00) >> 8) < afterCut.secondVector.greenMin) ? ((k.pixel & 0x0000ff00) >> 8) : afterCut.secondVector.greenMin;
        afterCut.secondVector.blueMax = ((k.pixel & 0x000000ff) > afterCut.secondVector.blueMax) ? (k.pixel & 0x000000ff) : afterCut.secondVector.blueMax;
        afterCut.secondVector.blueMin = ((k.pixel & 0x000000ff) < afterCut.secondVector.blueMin) ? (k.pixel & 0x000000ff) : afterCut.secondVector.blueMin;
      }
    }
    return;
  }
}

Pixel Sum(std::vector < PixelStat >const & bucket)
{
  double R = 0;
  double G = 0;
  double B = 0;
  unsigned long int n = 0;
  for (auto k : bucket)
  {
    //R = sqrt(pow(R, 2) + pow((k.pixel & 0x00ff0000) >> 16, 2))/2;
    //G = sqrt(pow(G, 2) + pow((k.pixel & 0x0000ff00) >> 8, 2))/2;
    //B = sqrt(pow(B, 2) + pow(k.pixel & 0x000000ff, 2))/2;
    R = R + ((k.pixel & 0x00ff0000) >> 16) * k.number;
    G = G + ((k.pixel & 0x0000ff00) >> 8) * k.number;
    B = B + (k.pixel & 0x000000ff) * k.number;
    n += k.number;
  }
  return { (unsigned char)(R / n), (unsigned char)(G / n), (unsigned char)(B / n) };
}

void spreadError(std::vector < std::vector <PixelError> > & matrixError, PixelError const & error, unsigned long int i, unsigned long int j)
{
  matrixError[i][j + 1].red += (5.0 / 32) * error.red;
  matrixError[i][j + 1].green += (5.0 / 32) * error.green;
  matrixError[i][j + 1].blue += (5.0 / 32) * error.blue;

  matrixError[i][j + 2].red += (3.0 / 32) * error.red;
  matrixError[i][j + 2].green += (3.0 / 32) * error.green;
  matrixError[i][j + 2].blue += (3.0 / 32) * error.blue;

  if (j >= 2)
  {
    matrixError[i + 1][j - 2].red += (2.0 / 32) * error.red;
    matrixError[i + 1][j - 2].green += (2.0 / 32) * error.green;
    matrixError[i + 1][j - 2].blue += (2.0 / 32) * error.blue;

    matrixError[i + 1][j - 1].red += (4.0 / 32) * error.red;
    matrixError[i + 1][j - 1].green += (4.0 / 32) * error.green;
    matrixError[i + 1][j - 1].blue += (4.0 / 32) * error.blue;

    matrixError[i + 2][j - 1].red += (2.0 / 32) * error.red;
    matrixError[i + 2][j - 1].green += (2.0 / 32) * error.green;
    matrixError[i + 2][j - 1].blue += (2.0 / 32) * error.blue;
  }

  matrixError[i + 1][j].red += (5.0 / 32) * error.red;
  matrixError[i + 1][j].green += (5.0 / 32) * error.green;
  matrixError[i + 1][j].blue += (5.0 / 32) * error.blue;

  matrixError[i + 2][j].red += (3.0 / 32) * error.red;
  matrixError[i + 2][j].green += (3.0 / 32) * error.green;
  matrixError[i + 2][j].blue += (3.0 / 32) * error.blue;

  matrixError[i + 1][j + 1].red += (4.0 / 32) * error.red;
  matrixError[i + 1][j + 1].green += (4.0 / 32) * error.green;
  matrixError[i + 1][j + 1].blue += (4.0 / 32) * error.blue;

  matrixError[i + 2][j + 1].red += (2.0 / 32) * error.red;
  matrixError[i + 2][j + 1].green += (2.0 / 32) * error.green;
  matrixError[i + 2][j + 1].blue += (2.0 / 32) * error.blue;

  matrixError[i + 1][j + 2].red += (2.0 / 32) * error.red;
  matrixError[i + 1][j + 2].green += (2.0 / 32) * error.green;
  matrixError[i + 1][j + 2].blue += (2.0 / 32) * error.blue;
}

int main()
{
  Header headerin("in.bmp");
  if (headerin.isError)
  {
    std::cout << "This is not BMP" << std::endl;
  }
  std::ifstream fin("in.bmp", std::ios::binary);
  unsigned char trash;
  for (unsigned int i = 0; i < headerin.bmpHeader.dataOffset; i++)
  {
    fin.read((char*)&trash, size_t(1));
  }
  std::vector < std::vector <Pixel> > pixels(headerin.infoHeader.height, std::vector <Pixel>(headerin.infoHeader.width));

  size_t trash_size = (headerin.infoHeader.width * (-3)) % 4;
  for (unsigned short i = 0; i < headerin.infoHeader.height; i++)
  {
    for (unsigned short j = 0; j < headerin.infoHeader.width; j++)
    {
      fin.read((char*)&(pixels[i][j].red), size_t(1));
      fin.read((char*)&(pixels[i][j].green), size_t(1));
      fin.read((char*)&(pixels[i][j].blue), size_t(1));
    }
    fin.read((char*)&trash, trash_size);
  }
  fin.close();

  // ----------------------------------------------------------------

  std::ofstream foutCopy = headerin.writeHeader("out_copy.bmp");
  trash = 0;
  for (unsigned short i = 0; i < headerin.infoHeader.height; i++)
  {
    for (unsigned short j = 0; j < headerin.infoHeader.width; j++)
    {
      foutCopy.write((char*)&(pixels[i][j].red), size_t(1));
      foutCopy.write((char*)&(pixels[i][j].green), size_t(1));
      foutCopy.write((char*)&(pixels[i][j].blue), size_t(1));
    }
    foutCopy.write((char*)&trash, trash_size);
  }
  foutCopy.close();

  // ----------------------------------------------------------------

  Header headerOut16(
    { 19778,                                                                                                       // signature
    54 + (2 * headerin.infoHeader.width + ((headerin.infoHeader.width * (-2)) % 4)) * headerin.infoHeader.height,  // fileSize
    0,                                                                                                             // rezerved
    54 },                                                                                                          // dataOffset
    { 40,                                                                                                          // size
    headerin.infoHeader.width,                                                                                     // width
    headerin.infoHeader.height,                                                                                    // height
    1,                                                                                                             // planes
    16,                                                                                                            // bitCount
    0,                                                                                                             // compression
    0,                                                                                                             // imageSize
    headerin.infoHeader.xPixelsPerM,                                                                               // xPixelsPerM
    headerin.infoHeader.yPixelsPerM,                                                                               // yPixelsPerM
    0,                                                                                                             // colorsUsed
    0 });                                                                                                          // colorImportant

  std::ofstream fout16 = headerOut16.writeHeader("out_16.bmp");
  trash = 0;
  trash_size = (headerOut16.infoHeader.width * (-2)) % 4;
  unsigned short pixel16;
  for (unsigned short i = 0; i < headerOut16.infoHeader.height; i++)
  {
    for (unsigned short j = 0; j < headerOut16.infoHeader.width; j++)
    {
      pixel16 = ((pixels[i][j].blue & 248) << 7) +
        ((pixels[i][j].green & 248) << 2) +
        ((pixels[i][j].red & 248) >> 3);
      fout16.write((char*)&pixel16, size_t(2));
    }
    fout16.write((char*)&trash, trash_size);
  }
  fout16.close();

  // ----------------------------------------------------------------

  Header headerOut8(
    { 19778,                                                                                                         // signature
    54 + 1024 + (headerin.infoHeader.width + ((headerin.infoHeader.width * (-1)) % 4)) * headerin.infoHeader.height, // fileSize
    0,                                                                                                               // rezerved
    54 + 1024 },                                                                                                     // dataOffset
    { 40,                                                                                                            // size
    headerin.infoHeader.width,                                                                                       // width
    headerin.infoHeader.height,                                                                                      // height
    1,                                                                                                               // planes
    8,                                                                                                               // bitCount
    0,                                                                                                               // compression
    0,                                                                                                               // imageSize
    headerin.infoHeader.xPixelsPerM,                                                                                 // xPixelsPerM
    headerin.infoHeader.yPixelsPerM,                                                                                 // yPixelsPerM
    0,                                                                                                               // colorsUsed
    0 });                                                                                                            // colorImportant

  std::ofstream fout8 = headerOut8.writeHeader("out_8.bmp");
  trash = 0;
  trash_size = (headerOut8.infoHeader.width * (-1)) % 4;
  unsigned char pixel8;
  unsigned char t = 0;
  for (int i = 0; i < 256; i++)
  {
    t = i & 224;
    fout8.write((char*)&t, size_t(1));
    t = (i & 28) << 3;
    fout8.write((char*)&t, size_t(1));
    t = (i & 3) << 6;
    fout8.write((char*)&t, size_t(1));
    fout8.write((char*)&trash, size_t(1));
  }
  for (unsigned short i = 0; i < headerOut8.infoHeader.height; i++)
  {
    for (unsigned short j = 0; j < headerOut8.infoHeader.width; j++)
    {
      pixel8 = ((pixels[i][j].blue & 192) >> 6) +
        ((pixels[i][j].green & 224) >> 3) +
        (pixels[i][j].red & 224);
      fout8.write((char*)&pixel8, size_t(1));
    }
    fout8.write((char*)&trash, trash_size);
  }
  fout8.close();

  //----------------------------------------------------------------

  Header headerOut4(
    { 19778,                                                                                                             // signature
    54 + 64 + (headerin.infoHeader.width / 2 + ((headerin.infoHeader.width * (-1) / 2) % 4)) * headerin.infoHeader.height, // fileSize
    0,                                                                                                                   // rezerved
    54 + 64 },                                                                                                           // dataOffset
    { 40,                                                                                                                // size
    headerin.infoHeader.width,                                                                                           // width
    headerin.infoHeader.height,                                                                                          // height
    1,                                                                                                                   // planes
    4,                                                                                                                   // bitCount
    0,                                                                                                                   // compression
    0,                                                                                                                   // imageSize
    headerin.infoHeader.xPixelsPerM,                                                                                     // xPixelsPerM
    headerin.infoHeader.yPixelsPerM,                                                                                     // yPixelsPerM
    0,                                                                                                                  // colorsUsed
    0 });                                                                                                                // colorImportant

  std::ofstream fout4 = headerOut4.writeHeader("out_4.bmp");
  trash = 0;
  trash_size = (headerOut4.infoHeader.width * (-1)) % 4;
  unsigned char pixel4;
  t = 0;
  for (int i = 0; i < 16; i++)
  {
    t = (i & 12) << 4;
    fout4.write((char*)&t, size_t(1));
    t = (i & 2) << 6;
    fout4.write((char*)&t, size_t(1));
    t = (i & 1) << 7;
    fout4.write((char*)&t, size_t(1));
    fout4.write((char*)&trash, size_t(1));
  }
  for (unsigned short i = 0; i < headerOut4.infoHeader.height; i++)
  {
    for (unsigned short j = 0; j < headerOut4.infoHeader.width; j += 2) //Todo: ׸� - �����
    {
      pixel4 = ((pixels[i][j].blue & 128) >> 3) +
        ((pixels[i][j].green & 128) >> 2) +
        (pixels[i][j].red & 192);
      pixel4 += ((pixels[i][j + 1].blue & 128) >> 7) +
        ((pixels[i][j + 1].green & 128) >> 6) +
        ((pixels[i][j + 1].red & 192) >> 4);
      fout4.write((char*)&pixel4, size_t(1));
    }
    fout4.write((char*)&trash, trash_size);
  }
  fout4.close();

  // ----------------------------------------------------------------
  
fin.open("in.bmp", std::ios::binary);
  for (unsigned int i = 0; i < headerin.bmpHeader.dataOffset; i++)
  {
    fin.read((char*)&trash, size_t(1));
  }

  std::vector < PixelStat > pixelStat;
  Pixel pixel{ 0,0,0 };
  trash_size = (headerin.infoHeader.width * (-3)) % 4;
  PixelMaxMin pixelMaxMin{ 0,255,0,255,0,255 };
  unsigned long int tPixel = 0;
  std::cout << "Analysis inpit image" << std::endl;
  for (unsigned short i = 0; i < headerin.infoHeader.height; i++)
  {
    for (unsigned short j = 0; j < headerin.infoHeader.width; j++)
    {
      pixel = { 0,0,0 };
      fin.read((char*)&(pixel.red), size_t(1));
      fin.read((char*)&(pixel.green), size_t(1));
      fin.read((char*)&(pixel.blue), size_t(1));
      pixelMaxMin.redMax = (pixel.red > pixelMaxMin.redMax) ? pixel.red : pixelMaxMin.redMax;
      pixelMaxMin.redMin = (pixel.red < pixelMaxMin.redMin) ? pixel.red : pixelMaxMin.redMin;
      pixelMaxMin.greenMax = (pixel.green > pixelMaxMin.greenMax) ? pixel.green : pixelMaxMin.greenMax;
      pixelMaxMin.greenMin = (pixel.green < pixelMaxMin.greenMin) ? pixel.green : pixelMaxMin.greenMin;
      pixelMaxMin.blueMax = (pixel.blue > pixelMaxMin.blueMax) ? pixel.blue : pixelMaxMin.blueMax;
      pixelMaxMin.blueMin = (pixel.blue < pixelMaxMin.blueMin) ? pixel.blue : pixelMaxMin.blueMin;

      tPixel = (pixel.red << 16) + (pixel.green << 8) + (pixel.blue);
      unsigned long int unique = 0;
      bool isExist = false;
      for (auto k : pixelStat)
      {
        if (k.pixel == tPixel)
        {
          pixelStat[unique].number++;
          isExist = true;
          break;
        }
        unique++;
      }
      if (!isExist)
      {
        pixelStat.push_back({ tPixel, 1 });
      }
    }
    fin.read((char*)&trash, trash_size);
  }
  fin.close();
  std::cout << "First cut" << std::endl;
  AfterCut afterCut;
  Cut(pixelStat, pixelMaxMin, afterCut);
  std::cout << "Second cut" << std::endl;
  AfterCut afterCut1;
  AfterCut afterCut2;
  Cut(afterCut.firstHalf, afterCut.firstVector, afterCut1);
  Cut(afterCut.secondHalf, afterCut.secondVector, afterCut2);
  std::cout << "Third cut" << std::endl;
  AfterCut afterCut11;
  AfterCut afterCut12;
  AfterCut afterCut21;
  AfterCut afterCut22;
  Cut(afterCut1.firstHalf, afterCut1.firstVector, afterCut11);
  Cut(afterCut1.secondHalf, afterCut1.secondVector, afterCut12);
  Cut(afterCut2.firstHalf, afterCut2.firstVector, afterCut21);
  Cut(afterCut2.secondHalf, afterCut2.secondVector, afterCut22);
  std::cout << "Fourth cut" << std::endl;
  AfterCut afterCut111;
  AfterCut afterCut112;
  AfterCut afterCut121;
  AfterCut afterCut122;
  AfterCut afterCut211;
  AfterCut afterCut212;
  AfterCut afterCut221;
  AfterCut afterCut222;
  Cut(afterCut11.firstHalf, afterCut11.firstVector, afterCut111);
  Cut(afterCut11.secondHalf, afterCut11.secondVector, afterCut112);
  Cut(afterCut12.firstHalf, afterCut12.firstVector, afterCut121);
  Cut(afterCut12.secondHalf, afterCut12.secondVector, afterCut122);
  Cut(afterCut21.firstHalf, afterCut21.firstVector, afterCut211);
  Cut(afterCut21.secondHalf, afterCut21.secondVector, afterCut212);
  Cut(afterCut22.firstHalf, afterCut22.firstVector, afterCut221);
  Cut(afterCut22.secondHalf, afterCut22.secondVector, afterCut222);

  std::cout << "Create palette" << std::endl;
  Pixel palette[16];
  palette[0] = Sum(afterCut111.firstHalf);
  palette[1] = Sum(afterCut111.secondHalf);
  palette[2] = Sum(afterCut112.firstHalf);
  palette[3] = Sum(afterCut112.secondHalf);
  palette[4] = Sum(afterCut121.firstHalf);
  palette[5] = Sum(afterCut121.secondHalf);
  palette[6] = Sum(afterCut122.firstHalf);
  palette[7] = Sum(afterCut122.secondHalf);
  palette[8] = Sum(afterCut211.firstHalf);
  palette[9] = Sum(afterCut211.secondHalf);
  palette[10] = Sum(afterCut212.firstHalf);
  palette[11] = Sum(afterCut212.secondHalf);
  palette[12] = Sum(afterCut221.firstHalf);
  palette[13] = Sum(afterCut221.secondHalf);
  palette[14] = Sum(afterCut222.firstHalf);
  //palette[0] = { 0,0,0 };
  palette[15] = Sum(afterCut222.secondHalf);
  Header headerOut4Quant(
    { 19778,                                                                                                             // signature
    54 + 64 + (headerin.infoHeader.width / 2 + ((headerin.infoHeader.width * (-1) / 2) % 4)) * headerin.infoHeader.height, // fileSize
    0,                                                                                                                   // rezerved
    54 + 64 },                                                                                                           // dataOffset
    { 40,                                                                                                                // size
    headerin.infoHeader.width,                                                                                           // width
    headerin.infoHeader.height,                                                                                          // height
    1,                                                                                                                   // planes
    4,                                                                                                                   // bitCount
    0,                                                                                                                   // compression
    0,                                                                                                                   // imageSize
    headerin.infoHeader.xPixelsPerM,                                                                                     // xPixelsPerM
    headerin.infoHeader.yPixelsPerM,                                                                                     // yPixelsPerM
    0,                                                                                                                  // colorsUsed
    0 });                                                                                                                // colorImportant

  std::ofstream fout4Quant = headerOut4Quant.writeHeader("out_4_quant.bmp");
  trash = 0;
  trash_size = (headerOut4Quant.infoHeader.width * (-1)) % 4;
  unsigned char pixel4Quant;
  for (int i = 0; i < 16; i++)
  {
    fout4Quant.write((char*)&(palette[i].red), size_t(1));
    fout4Quant.write((char*)&(palette[i].green), size_t(1));
    fout4Quant.write((char*)&(palette[i].blue), size_t(1));
    fout4Quant.write((char*)&trash, size_t(1));
  }
  std::vector < std::vector <PixelError> > error(headerin.infoHeader.height + 2, std::vector <PixelError>(headerin.infoHeader.width + 2));
  for (unsigned short i = 0; i < headerOut4Quant.infoHeader.height + 2; i++)
  {
    for (unsigned short j = 0; j < headerOut4Quant.infoHeader.width + 2; j++)
    {
      error[i][j] = { 0, 0, 0 };
    }
  }
  for (unsigned short i = 0; i < headerOut4Quant.infoHeader.height; i++)
  {
    for (unsigned short j = 0; j < headerOut4Quant.infoHeader.width; j += 2) //Todo: ׸� - �����
    {
      double min = 0xffff;
      unsigned char min_i = 0;
      double delta = 0;
      for (int k = 0; k < 16; k++)
      {
        delta = 0.2126*pow(palette[k].red - (pixels[i][j].red + error[i][j].red), 2) +
          0.7152*pow(palette[k].green - (pixels[i][j].green + error[i][j].green), 2) +
          0.0722*pow(palette[k].blue - (pixels[i][j].blue + error[i][j].blue), 2);
        if (delta <= min)
        {
          min = delta;
          min_i = k;
        }
      }
      PixelError tError{ pixels[i][j].red - palette[min_i].red,
        pixels[i][j].green - palette[min_i].green,
        pixels[i][j].blue - palette[min_i].blue };
      spreadError(error, tError, i, j);
      pixel4Quant = min_i << 4;
      min = 0xffff;
      min_i = 0;
      delta = 0;
      for (int k = 0; k < 16; k++)
      {
        delta = 0.2126*pow(palette[k].red - (pixels[i][j + 1].red + error[i][j + 1].red), 2) +
          0.7152*pow(palette[k].green - (pixels[i][j + 1].green + error[i][j + 1].green), 2) +
          0.0722*pow(palette[k].blue - (pixels[i][j + 1].blue + error[i][j + 1].blue), 2);
        if (delta <= min)
        {
          min = delta;
          min_i = k;
        }
      }
      pixel4Quant += min_i;
      tError = { pixels[i][j + 1].red - palette[min_i].red,
        pixels[i][j + 1].green - palette[min_i].green,
        pixels[i][j + 1].blue - palette[min_i].blue };
      spreadError(error, tError, i, j + 1);
      fout4Quant.write((char*)&pixel4Quant, size_t(1));
    }
    fout4Quant.write((char*)&trash, trash_size);
  }
  fout4Quant.close();

  return 0;
}