#ifndef HEADERS
#define HEADERS

#include <fstream>

struct BmpHeader
{
  unsigned short signature;
  unsigned long int fileSize;
  unsigned long int reserved;
  unsigned long int dataOffset;
};

struct InfoHeader
{
  unsigned long int size;
  unsigned long int width;
  unsigned long int height;
  unsigned short planes;
  unsigned short bitCount;
  unsigned long int compression;
  unsigned long int imageSize;
  unsigned long int xPixelsPerM;
  unsigned long int yPixelsPerM;
  unsigned long int colorsUsed;
  unsigned long int colorImportant;
};

class Header
{
public:
  Header(std::string);
  Header(struct BmpHeader, struct InfoHeader);
  std::ofstream writeHeader(std::string);

  struct BmpHeader bmpHeader;
  struct InfoHeader infoHeader;
  bool isError;
};

#endif // !HEADERS