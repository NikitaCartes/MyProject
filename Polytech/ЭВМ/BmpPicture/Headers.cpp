#include "Headers.hpp"

Header::Header(std::string filename)
{
  std::ifstream fin(filename, std::ios::binary);
  isError = false;
  fin.read((char*)&bmpHeader.signature, size_t(2));
  if (bmpHeader.signature != 19778)
  {
    isError = true;
    return;
  }
  fin.read((char*)&bmpHeader.fileSize, size_t(4));
  fin.read((char*)&bmpHeader.reserved, size_t(4));
  fin.read((char*)&bmpHeader.dataOffset, size_t(4));

  fin.read((char*)&infoHeader.size, size_t(4));
  fin.read((char*)&infoHeader.width, size_t(4));
  fin.read((char*)&infoHeader.height, size_t(4));
  fin.read((char*)&infoHeader.planes, size_t(2));
  fin.read((char*)&infoHeader.bitCount, size_t(2));
  fin.read((char*)&infoHeader.compression, size_t(4));
  fin.read((char*)&infoHeader.imageSize, size_t(4));
  fin.read((char*)&infoHeader.xPixelsPerM, size_t(4));
  fin.read((char*)&infoHeader.yPixelsPerM, size_t(4));
  fin.read((char*)&infoHeader.colorsUsed, size_t(4));
  fin.read((char*)&infoHeader.colorImportant, size_t(4));
  fin.close();
};

Header::Header(BmpHeader bmpHeaderInput, InfoHeader infoHeaderInput)
{
  bmpHeader.signature = bmpHeaderInput.signature;
  bmpHeader.fileSize = bmpHeaderInput.fileSize;
  bmpHeader.reserved = bmpHeaderInput.reserved;
  bmpHeader.dataOffset = bmpHeaderInput.dataOffset;

  infoHeader.size = infoHeaderInput.size;
  infoHeader.width = infoHeaderInput.width;
  infoHeader.height = infoHeaderInput.height;
  infoHeader.planes = infoHeaderInput.planes;
  infoHeader.bitCount = infoHeaderInput.bitCount;
  infoHeader.compression = infoHeaderInput.compression;
  infoHeader.imageSize = infoHeaderInput.imageSize;
  infoHeader.xPixelsPerM = infoHeaderInput.xPixelsPerM;
  infoHeader.yPixelsPerM = infoHeaderInput.yPixelsPerM;
  infoHeader.colorsUsed = infoHeaderInput.colorsUsed;
  infoHeader.colorImportant = infoHeaderInput.colorImportant;
};

std::ofstream Header::writeHeader(std::string filename)
{
  std::ofstream fout(filename, std::ios::binary);

  fout.write((char*)&bmpHeader.signature, size_t(2));
  fout.write((char*)&bmpHeader.fileSize, size_t(4));
  fout.write((char*)&bmpHeader.reserved, size_t(4));
  fout.write((char*)&bmpHeader.dataOffset, size_t(4));

  fout.write((char*)&infoHeader.size, size_t(4));
  fout.write((char*)&infoHeader.width, size_t(4));
  fout.write((char*)&infoHeader.height, size_t(4));
  fout.write((char*)&infoHeader.planes, size_t(2));
  fout.write((char*)&infoHeader.bitCount, size_t(2));
  fout.write((char*)&infoHeader.compression, size_t(4));
  fout.write((char*)&infoHeader.imageSize, size_t(4));
  fout.write((char*)&infoHeader.xPixelsPerM, size_t(4));
  fout.write((char*)&infoHeader.yPixelsPerM, size_t(4));
  fout.write((char*)&infoHeader.colorsUsed, size_t(4));
  fout.write((char*)&infoHeader.colorImportant, size_t(4));
  //fout.close();
  return fout;
}